<?php
    /**
     * В качестве автозагрузчика и менеджера расширений используется Composer, это его автозагрузчик
     * @see composer.json
     */
    require_once('vendor/autoload.php');

    /**
     * Константа для включаемых файлов, обозначающая, что мы попали к нему через точку входа
     */
    define('ASTRO', true);

    /**
     * Инклудим конфигурационный файл, где проходит первичное объявление нужных переменных
     */
    try {
        require_once('config.php');
    } catch(Exception $e) {
        die($e->getMessage());
    }

    /**
     * Берём главную базу данных и выбираем там коллекцию sessions, после чего определяем её
     * как хранилище для сессий
     * Для Apache не надо, всё локально
     * @todo Возможно, перевести это всё дело на кэш Redis, но необязательно
     */
    use Altmetric\MongoSessionHandler;
    if(php_sapi_name() != "apache2handler") {
        $sessions = \Astro\Mods::$db->select('sessions');
        $handler = new MongoSessionHandler($sessions);
        session_set_save_handler($handler);
        session_set_cookie_params(86400, '/', ASTRO_DOMAIN_NAME, false, true);
    }
    session_name('sid');
    session_start();

    /**
     * Определяем, какие контроллеры использовать
     * На админском домене используются другие
     */
    $c = (ASTRO_DOMAIN_NAME == 'admin.astronaut.kz') ? 'admin_controller' : 'controller';

    /**
     * В файле конфигурации веб-сервера Azure указана перезапись всех запросов на данный файл (index.php)
     * В GET-параметре route передаётся собственно полный роут. Здесь мы его определяем
     *
     * Если же речь про Apache - достаём всё из запроса
     *
     * @see web.config
     * @see .htaccess
     */
    if(php_sapi_name() == "apache2handler" || php_sapi_name() == "fpm-fcgi") {
        $route = $_SERVER['REQUEST_URI'];

        if (strpos($route, '?') !== false)
            $route = strstr($route, '?', true);

        if (strpos($route, '/') === 0)
            $route = substr($route, 1);

        if (!strlen($route))
            $route = 'index';
    }
    else
        $route = isset($_REQUEST['route']) ? $_REQUEST['route'] : 'index';

    /**
     * Добавляем поддержку адресов в духе astronaut.kz/webhook_1.php
     * и меняем роут для webhook. Нам это необходимо, чтобы несколько инстансов подключались к одному
     * экземпляру Астронавта
     */
    if(strpos($route, 'webhook_') === 0 && preg_match('/^webhook_([0-9]+).php$/', $route, $matches)) {
        $_use_webhook_instance = $matches[1];
        $route = 'webhook';
    }

    /**
     * Определяем нужный нам контроллер с нужной нам папки
     * Имя контроллера совпадает с роутом, который указан в адресной строке
     * Например: bb.astronaut.kz/test перенаправит на контроллер controller/test.php
     * Если делать запрос на админку (admin.astronaut.kz), контроллер будет браться из папки admin_controller
     */
    try {
        $page = __DIR__ . '/' . $c . '/' . $route . '.php';
    } catch(Exception $e) {
        \Astro\Mods::$messages->send_debug_message( (string)$e );
    }

    // Для админки подключаем файлы header и footer с дизайном шапки и подвала. В других случаях это не надо
    if($c == 'admin_controller')
        require_once(__DIR__ . '/admin_controller/_header.php');

    // Подключаем ранее определённый контроллер, ошибки в случае чего отправляем в отладочный канал
    try {
        require_once(file_exists($page) ? $page : 'controller/404.php');
    } catch(\Astro\AccessException $e) {
        print("<h2>403: Ошибка доступа</h2>"); // @todo отдельный шаблон
    } catch(Exception $e) {
        print("<h2>Возникла внутренняя ошибка сервера</h2>"); // @todo отдельный шаблон
        \Astro\Mods::$messages->send_debug_message("#exception #astronaut ", (string)$e);
    }

    if($c == 'admin_controller')
        require_once(__DIR__ . '/admin_controller/_footer.php');