<?php
    if(php_sapi_name() !== 'cli')
        die('Forbidden');

    if($_SERVER['argc'] <= 1) {
        echo 'No site specified';
        return;
    }

    if($_SERVER['argc'] <= 2) {
        echo 'No task specified';
        return;
    }

    chdir(__DIR__);
    require_once('vendor/autoload.php');
    define('ASTRO', true);

    try {
        require_once('config.php');
    } catch(Exception $e) {
        die($e->getMessage());
    }

    set_time_limit(600);

    try {
        $debug = [];
        $ms = new \Astro\Multisite();

        foreach($ms->sites as $url => $params) {
            if($url !== $_SERVER['argv'][1])
                continue;

            $info = ["🛠  Задача #".$_SERVER['argv'][2]." #{$url}:"];

            $ms = new \Astro\Multisite($url);
            $bitrix = $ms->getBitrix();
            $api = $ms->getChatApi();
            $db = new \Astro\DB(_DB_CONNECTION, _generate_db_name($url));

            if(strpos($_SERVER['argv'][2], 'task_') === 0)
            {
                switch($_SERVER['argv'][2])
                {
                    case 'task_leads':
                        if($cnt = $bitrix->leadsUpdate())
                            \Astro\Mods::$messages->send_debug_message("🛒 {$url}: ".$cnt." новых лидов");
                        return;

                    case 'task_managers':
                        \Astro\Mods::$messages->send_debug_message("🛒 {$url}: ".$bitrix->managersUpdate()." менеджеров проверено");
                        return;

                    case 'task_number_prgn':
                        $file = file_get_contents('contact.csv');
                        $rows = explode(PHP_EOL, $file);

                        foreach($rows as $k => $r) {
                            if($k == 0)
                                continue;

                            $r = explode(';', $r);
                            $num = trim($r[5], '"');

                            if(!strlen($num) || !isset($r[5]))
                                continue;

                            $num = preg_replace("/[^0-9\.]/", '', $num);

                            $repeats = $db->count('calls', ['PHONE_NUMBER' => $num]);

                            if($repeats < 2) {
                                file_put_contents('Необработанные.csv', $num . PHP_EOL, FILE_APPEND);
                                echo "{$num} - BAD ({$repeats})\n";
                            }
                            else {
                                file_put_contents('Обработанные.csv', $num . PHP_EOL, FILE_APPEND);
                                echo "{$num} - good ({$repeats})\n";
                            }
                        }

                        return;


                    case 'task_test':
                        var_dump(
                            $bitrix->queryForever('crm.lead.get', ['id' => 100])
                        );
                        return;

                    case 'task_timers':
                        $timers = 0;

                        foreach($db->find('timers', ['time' => ['$lte' => time()]])->toArray() as $arr) {
                            $arr = (array)$arr;

                            if(isset($arr['instance']) && $arr['instance'] != $ms->getIID()) {
                                $inst = \Astro\Mods::$adb->findOne('instance', ['id' => $arr['instance']]);
                                $ca = new \Astro\ChatApi($inst->token, $inst->url);
                            }
                            else
                                $ca = &$api;

                            $results = var_export($ca->sendPhoneMessage( $arr['num'], $arr['text']), true);
                            $datetime = date('r', $arr['time']);
                            \Astro\Mods::$messages->send_debug_message("⏰ {$url}: Таймер на {$datetime}\n{$results}");

                            $db->delete('timers', $arr);
                            $timers++;
                        }

                        if($timers)
                            \Astro\Mods::$messages->send_debug_message("⏰ {$url}: {$timers} таймеров отправлено");
                        return;

                    case 'task_calls_all':
                        if($cnt = $bitrix->callsHistoryUpdate())
                            \Astro\Mods::$messages->send_debug_message("☎ {$url}: ".$cnt." старых звонков добавлено");
                        return;

                    case 'task_calls':
                        if($cnt = $bitrix->callsUpdate())
                            \Astro\Mods::$messages->send_debug_message("☎ {$url}: ".$cnt." новых звонков");
                        return;

                    case 'task_deals':
                        if($cnt = $bitrix->dealsUpdate())
                            \Astro\Mods::$messages->send_debug_message("💵 {$url}: ".$cnt." новых сделок");
                        return;

                    case 'task_doubles':
                        $leads = $bitrix->getList('crm.lead.list', ['filter' => [
                            'STATUS_ID' => 'NEW',
                            '>DATE_CREATE' => new \DateTime('-1 hour')
                        ]]);

                        print(count($leads['result']) . ' leads to check' . PHP_EOL);

                        $found_duplicates = [];

                        foreach($leads['result'] as $el) {
                            $el = (object)$el;
                            $duplicates = [];

                            print('Checking lead #' . $el->ID . PHP_EOL);

                            foreach(['CONTACT_ID', 'EMAIL', 'PHONE', 'ORIGIN_ID', 'COMPANY_ID'] as $field) {
                                if(!isset($el->{$field}) || !strlen($el->{$field}))
                                    continue;

                                if($field === 'PHONE') {
                                    foreach($el->PHONE as $ph) {
                                        if(!isset($ph['value']) || !strlen($ph['value']))
                                            continue;

                                        $tryFind = $bitrix->queryForever('crm.lead.list', ['filter' => [
                                            'PHONE' => $ph['value'],
                                            'STATUS_ID' => 'NEW'
                                        ]]);

                                        break;
                                    }
                                } else {
                                    $tryFind = $bitrix->queryForever('crm.lead.list', ['filter' => [
                                        $field => $el->{$field},
                                        'STATUS_ID' => 'NEW'
                                    ]]);
                                }

                                if(is_array($tryFind) && isset($tryFind['result']) && count($tryFind['result']))
                                    $duplicates = array_merge($duplicates, $tryFind['result']);
                            }

                            $sc_for_history = '';
                            $old_dupli = preg_match('/оставил (.+) заявок/', $el->TITLE, $match);

                            if(isset($match[1]) && is_numeric($match[1]))
                                $leads_cnt = $match[1];
                            else
                                $leads_cnt = 0;

                            $newtitle = preg_replace('/\(оставил.+\)/', '', $el->TITLE);
                            $a_title = '';

                            foreach($duplicates as $sc) {
                                $sc = (object)$sc;

                                if($sc->ID == $el->ID || $sc->STATUS_ID !== 'NEW')
                                    continue;

                                $sc_for_history .= $sc->TITLE . ' - ' . $sc->DATE_CREATE . ';' . PHP_EOL;

                                \Astro\Mods::$messages->send_debug_message('Удален дубликат для `'.$el->TITLE.'` под названием `'.$sc->TITLE.'` ', 'centrus');

                                $bd = $bitrix->queryForever('crm.lead.delete', ['id' => $sc->ID]);
                                $dd = $db->delete('leads', ['ID' => $sc->ID]);
                                $found_duplicates[] = $sc->ID;

                                $leads_cnt++;

                                $diff = (strtotime($el->DATE_CREATE) - strtotime($sc->DATE_CREATE)) / 86400;

                                if($diff < 3)
                                    $a_title = 'α';
                                elseif($diff < 10)
                                    $a_title = 'αα';
                                else
                                    $a_title = 'ααα';

                                print('Deleted duplicate ('.$el->ID.' -> '.$sc->ID.'): ');
                                print(PHP_EOL);
                            }

                            $newtitle .= ' (оставил '.$leads_cnt.' заявок, '.$a_title.')';

                            if(isset($el->UF_CRM_1547191458) && strlen($el->UF_CRM_1547191458))
                                $sc_for_history = $el->UF_CRM_1547191458 . $sc_for_history;

                            if($leads_cnt > 0) {
                                $bitrix->queryForever('crm.lead.update', [
                                    'id' => $el->ID,
                                    'fields' => [
                                        'TITLE' => $newtitle,
                                        'UF_CRM_1547191458' => $sc_for_history,
                                        'UF_CRM_1547198967260' => 1
                                    ]
                                ]);
                            } else {
                                $bitrix->queryForever('crm.lead.update', [
                                    'id' => $el->ID,
                                    'fields' => [
                                        'UF_CRM_1547198967260' => 1
                                    ]
                                ]);
                            }
                        }

                        if(count($found_duplicates))
                            \Astro\Mods::$messages->send_debug_message('🗑 Удалено '.count($found_duplicates).' дубликатов: ' . implode(', ', $found_duplicates), 'centrus');

                        return;

                    default:
                        print('Тип задачи не поддерживается');
                        return;
                }
            }

            $task = $db->findOne('funnel_tasks', ['_id' => new \MongoDB\BSON\ObjectId($_SERVER['argv'][2])]);
            if(!is_object($task) && !isset($task->_id)) {
                $info[] = "Задача не найдена :(";
                break;
            }
            $update = [];

            switch($task->type)
            {
                case 'funnel': // @todo оптимизировать это дело; в классах всё чистенько и правильно, а здесь лёгкая помойка
                    set_time_limit(3600*12);
                    $db->update('funnel_tasks', ['_id' => $task->_id], ['status' => 1]);

                    if(isset($task->from))
                        $from = $task->from;
                    else
                        $from = '-1 week';

                    if(isset($task->to))
                        $to = $task->to;
                    else
                        $to = 'now';

                    $html = "<table border=1>\n<thead>\n<th>Показатели</th>\n";
                    foreach(\Astro\Funnel\Base::$rows as $title => $class)
                        $html .= "<th>{$title}</th>\n";
                    $html .= "</thead>\n";
                    $html .= "<tbody>\n";
                    foreach(\Astro\Funnel\Base::$cols as $index => $colname) {
                        $html .= "<tr>\n<td>{$colname}</td>\n";
                        $function_name = \Astro\Funnel\Base::$funcs[$index];

                        foreach (\Astro\Funnel\Base::$rows as $title => $class) {
                            $cn = '\\Astro\\Funnel\\' . $class;
                            $obj = new $cn($db, $from, $to);
                            $int = call_user_func([$obj, $function_name]);

                            if(is_numeric($int) || strlen($int))
                                $html .= "<td>{$int}</td>\n";
                            else
                                $html .= "<td>???</td>\n";

                            unset($obj);
                        }
                        $html .= "</tr>\n";
                    }
                    $html .= "</tbody>\n</table>\n";
                    echo $html;

                    $update['result'] = base64_encode($html);
                    $update['status'] = 5;
                    $info[] = 'Задача выполнена успешно';
                    break;

                default:
                    $info[] = "Тип задачи не поддерживается";
                    $update['status'] = -1;
                    break;
            }
            #$classes = $ms->getClasses($db);
            #$api = $classes['api'];
            #$bitrix = $classes['bitrix'];

            if(count($update))
                $db->update('funnel_tasks', ['_id' => $task->_id], $update);

            $debug[] = implode(PHP_EOL, $info);
            unset($info, $classes, $api, $bitrix, $db);
            break;
        }

        if(count($debug)) {
            var_dump($debug);
            \Astro\Mods::$messages->send_debug_message(implode("\n\n", $debug));
        }
        else
            print("Empty result");
    }
    catch(Exception $e) {
        print($e);
        \Astro\Mods::$messages->send_debug_message("❌ Ошибка #tasks: \n\n" . (string)$e);
    }
