<?php
    if(php_sapi_name() !== 'cli')
        die('Forbidden');

    if($_SERVER['argc'] <= 1 && file_exists('cron_lock.txt') && file_get_contents('cron_lock.txt') >= time()) {
        echo 'Process locked';
        return;
    }

    file_put_contents('cron_lock.txt', time()+115);
    print('Preparing environment' . PHP_EOL);

    chdir(__DIR__);
    function execInBackground($cmd) {
        if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
            pclose(popen("start /B ". $cmd, "r"));
        else
            exec($cmd . " > /dev/null &");
    }

    require_once('vendor/autoload.php');
    define('ASTRO', true);

    try {
        require_once('config.php');
    } catch(Exception $e) {
        die($e->getMessage());
    }

    print('Starting main loop' . PHP_EOL);

    try {
        $ms = new \Astro\Multisite();
        $tasks = 0;

        foreach($ms->sites as $url => $params) {
            if($url == 'admin.astronaut.kz')
                continue;

            $ms = new \Astro\Multisite($url);
            $db = new \Astro\DB(_DB_CONNECTION, _generate_db_name($url));

            foreach($db->find('funnel_tasks', ['status' => ['$in' => [0, '0', false]]]) as $task) {
                execInBackground('php taskmgr.php '.$url.' '.$task->_id);
                $tasks++;
            }

            foreach(['task_doubles', 'task_leads', 'task_calls', 'task_deals', 'task_timers'] as $task) {
                execInBackground('php taskmgr.php '.$url.' '.$task);
                $tasks++;
            }

            /*$mess = new \Astro\Messages($params[0], $db);
            $inbox = $api->getInbox();
            $inbox_cnt = 0;
            if(is_array($inbox)) {
                foreach ($inbox as $i) {
                    if ($res = $mess->handleNew($i))
                        $inbox_cnt++;
                }
            }
            $info[] = "✉️ {$inbox_cnt} новых сообщений обработано";*/

            if(date('Hi') == '55' || date('Hi') == '54') {
                execInBackground('php taskmgr.php ' . $url . ' task_managers');
                $tasks++;
            }
        }

        if(count($debug)) {
            var_dump($debug);
            \Astro\Mods::$messages->send_debug_message("☑️ #cron-задачи: запущено {$tasks} процессов");
        }
        else
            print("Empty result");
    }
    catch(Exception $e) {
        print($e);
        \Astro\Mods::$messages->send_debug_message("❌ Ошибка #cron: \n\n" . (string)$e);
    }