<?php

namespace Astro;

/**
 * Класс для работы с сообщениями WhatsApp и их обработки
 * @package Astro
 */
class Messages
{
    /**
     * @var DB|null
     */
    protected $db = null;

    /**
     * @var DB|null
     */
    protected $adb = null;

    /**
     * @var \MongoDB\BSON\ObjectId|null
     */
    protected $id = null;

    /**
     * @var int
     */
    protected $instance;

    /**
     * Готовит "почву" для работы
     * @param int $instance - инстанс
     * @param null $db - объект БД (необязательно указывать)
     */
    public function __construct(int $instance = 0, $db = null)
    {
        if(!is_object($db))
            $db = &Mods::$db;

        $this->db = $db;
        $this->adb = &Mods::$adb;
        $this->instance = (string)$instance;
    }

    /**
     * Устаревший метод для определения, есть ли автоответ для определённого сообщения
     * @deprecated
     * @param $text
     * @return null|string
     * @deprecated
     */
    public function hasAuto($text)
    {
        return false;
    }

    /**
     * Получить список диалогов по критериям
     * @param int $offset
     * @param array $query
     * @return array
     */
    public function getDialogs(int $offset = 0, array $query = [])
    {
        $query['instance'] = (string)$this->instance;
        return $this->db->find('dialogs', $query, ['limit' => $limit=20, 'skip' => $offset*$limit, "sort" => ["time" => -1]])->toArray();
    }

    /**
     * Получить число непрочитанных инстанса
     * @return int
     */
    public function getUnread()
    {
        return $this->db->count('dialogs', ['answered' => false, 'instance' => (string)$this->instance]);
    }

    /**
     * Получить список входящих
     * @param int $offset
     * @param array $query
     * @return \MongoDB\Driver\Cursor
     */
    public function getInbox(int $offset = 0, array $query = [])
    {
        $query['instance'] = (string)$this->instance;

        if($offset > 0)
            $query = array_merge(['offset' => ['$gt' => $offset]], $query);

        return $this->db->find('messages', $query, ['limit' => 500, "sort" => ["time" => -1]]);
    }

    /**
     * Возвращает количество сообщений в диалоге с определённым номером телефона
     * @param string $num - номер телефона для проверки
     * @return bool
     */
    public function hasMessages(string $num)
    {
        $cnt = $this->db->count('messages', ['chatId' => new \MongoDB\BSON\Regex($num), 'instance' => (string)$this->instance], ['limit' => 1, "sort" => ["time" => -1]]);
        return ($cnt > 1);
    }

    /**
     * Изредка (например, при переносе) сообщения не складываются в диалоги
     * Данная функция чинит их
     * @return int
     */
    public function updateDialogs()
    {
        $mess = $this->db->find('messages', [])->toArray();
        echo count($mess) . " messages\n";

        $created = 0;

        foreach($mess as $v) {
            echo $v['chatId'] . " is handled\n";

            $this->db->upsert('dialogs', ['chatId' => $v['chatId']], [
                'name' => $v['senderName'],
                'answered' => $v['fromMe'],
                'offset' => isset($v['offset']) ? $v['offset'] : 0,
                'last' => mb_substr($v['body'], 0, 50),
                'lastFull' => $v['body'],
                'instance' => $v['instance'] ?? '12736',
                'chatId' => $v['chatId'],
                'time' => $v['time'],
                'date' => date('r', $v['time']),
                'num' => '+' . (int)filter_var($v['chatId'], FILTER_SANITIZE_NUMBER_INT)
            ]);
            $created++;
        }

        return $created;
    }

    /**
     * Случайным образом выбирает менеджера из списка и возвращает его ID
     * На практике помогает распределять лиды более-менее равномерно
     * @return mixed
     */
    public function getRandomManager()
    {
        if(ASTRO_DOMAIN_NAME === 'bb.astronaut.kz')
            return 4271;

        $mgr = [];
        foreach($this->db->find('managers', []) as $m)
            $mgr[] = (array)$m;
        $mgr = $mgr[ rand(0, count($mgr)) ];
        return $mgr['ID'];
    }

    /**
     * Жирная функция-прокладка для обработки новых входящих сообщений
     * Используется в вебхуках и cron
     * @param array $v - массив с данными сообщения
     * @param bool $insert - определяет, нужно ли добавить в сообщение в базу или только обработать
     * @param int $instance - инстанс, с которого пришло сообщение
     * @return bool
     */
    public function handleNew(array $v = [], bool $insert = true, int $instance = 0)
    {
        /*if(!function_exists('pcntl_fork')) {
            $this->send_debug_message('**Ошибка**. Функции подпроцессов здесь не разрешены. Пробую работать без подпроцессов');
        }
        else {
            try {
                $pid = pcntl_fork();

                if ($pid == -1) {
                    $this->send_debug_message('**ОЧЕНЬ-ОЧЕНЬ КРИТИЧЕСКАЯ ОШИБКА!** Не удалось породить дочерний процесс.');
                    return false;
                } else if ($pid) {
                    return !$v['fromMe'];
                }
            } catch (\Exception $e) {
                $this->send_debug_message('**ОЧЕНЬ-ОЧЕНЬ КРИТИЧЕСКАЯ ОШИБКА!** Не удалось породить дочерний процесс.');
                return false;
            }
        }*/

        // В данном массиве будет находиться отладочная информация, которая затём отправится в отладочный чат
        $debug = [];

        // Если не передан инстанс - определяем сами по текущему домену
        if($instance == 0)
            $instance = $this->instance;

        $debug[] = ASTRO_DOMAIN_NAME . ", инстанс {$instance}\nВходящее: `{$v['body']}`";

        // Если сообщение уже существует, и мы не обрабатываем старое умышленно - выходим из функции
        if($insert == true && $this->check($v['id']))
            return false;
        else {
            $cnt_messages = $this->db->count('messages', ['chatId' => $v['chatId'], 'instance' => $instance]);
            $debug[] = "Сообщений в диалоге: {$cnt_messages}";

            if($insert == true) {
                $v['instance'] = $instance;
                $this->insert($v); // вставляем сообщение в базу данных

                // обновляем или создаём диалог, которому принадлежит сообщение
                $this->db->upsert('dialogs', ['chatId' => $v['chatId'], 'instance' => $instance], [
                    'name' => $v['senderName'],
                    'instance' => $instance,
                    'answered' => $v['fromMe'],
                    'offset' => isset($v['offset']) ? $v['offset'] : 0,
                    'last' => mb_substr($v['body'], 0, 50),
                    'lastFull' => $v['body'],
                    'chatId' => $v['chatId'],
                    'time' => $v['time'],
                    'date' => date('r', $v['time']),
                    'num' => '+' . (int)filter_var($v['chatId'], FILTER_SANITIZE_NUMBER_INT)
                ]);

                $debug[] = "Обновлен диалог {$v['chatId']}";
            }

            // исходящие сообщения обрабатывать не надо, поэтому если оно является таковым - прерываем
            if($v['fromMe'] == true)
                return false;

            // получаем информацию об инстансе, на который пришло сообщение
            $info = $this->adb->findOne('instance', ['id' => ['$in' => [(string)$instance, (int)$instance]]]);

            // если нет данных по инстансу, мы не сможем его обработать, поэтому прерываем
            if(!is_object($info))
                return false;

            // извлекаем из ID чата только номер телефона
            $clinum = (int)filter_var($v['chatId'], FILTER_SANITIZE_NUMBER_INT);

            // готовим класс ChatApi согласно сохранённым в базе URL и токену
            $api = new ChatApi($info->token, $info->url);

            // если каким-либо инстансам нужны собственные отдельные обработчики - вписываем сюда согласно примеру
            /*if($instance == 17114) {
                return $this->bb_alaman_bot($api, $v);
            }*/

            // получаем все данные по настройках инстанса
            $info_arr = (array)$info;
            $auto_flags = (isset($info_arr['flags'])) ? (array)$info_arr['flags'] : []; // флаги
            $auto_vars = (isset($info_arr['vars'])) ? (array)$info_arr['vars'] : []; // переменные
            $auto_inf = isset($info_arr['set']) ? (array)$info_arr['set'] : []; // автоответчики
            $auto_timers = isset($info_arr['timers']) ? (array)$info_arr['timers'] : []; // таймеры

            // сообщение "тест" всегда возвращает статический отет
            if($v['body'] === 'тест' || $v['body'] === 'Ok!')
                $ans = 'Автоответчик работает';
            elseif(!isset($auto_inf[$v['body']])) {
                // если нету автоответа для нашего сообщения - определяем нужду в приветствии
                if($cnt_messages == 0 && isset($auto_vars['__']) && $auto_vars['__']['greeting']) {
                    // если диалог новый - активируем приветствие
                    $debug[] = 'Приветствие активировано';
                    $ans = mb_strlen($auto_vars['__']['greeting']) ? $auto_vars['__']['greeting'] : null;
                } else
                    $ans = null;
            }
            else // если же есть автоответчик - отвечаем согласно нму
                $ans = $auto_inf[$v['body']];

            $debug[] = ("Предполагаемый автоответ: `" . (is_null($ans) ? "отсутствует" : $ans) . "`");
            $debug[] = "Номер: {$clinum}";

            if(is_null($ans)) {
                $bitrix = &Mods::$bitrix;
                $send = [];
                $leads = $bitrix->queryForever('crm.lead.list', ['filter' => [
                    "PHONE" => $clinum
                ]]);

                foreach($leads['result'] as $l) {
                    if(isset($l['ASSIGNED_BY_ID']))
                        $send[ $l['ASSIGNED_BY_ID'] ] = 'Новое сообщение в лиде '.$l['TITLE'].': https://biznesbastau2018.bitrix24.kz/crm/lead/details/'.$l['ID'].'/';
                }

                foreach($send as $id => $text) {
                    $result = $bitrix->query('im.notify', ['to' => $id, 'message' => $text, 'type' => 'SYSTEM']);

                    if(isset($result['result']) && $pushid = $result['result'])
                        $debug[] = "🙉 ID оповещения менеджеру #".$id.": {$pushid}";
                }
            }

            if(is_array($auto_timers) && isset($auto_timers['__'])) {
                $auto_timers = $auto_timers['__'];
            }

            // запоминаем все таймеры в базу; когда придёт их час - крон всё сделает сам
            foreach($auto_timers as $kk => $t) {
                if(isset($t['time']) && mb_strlen($t['text'])) {
                    $result = $this->db->upsert('timers', ['num' => $clinum, 'instance' => $instance, 'text' => $t['text']], ['num' => $clinum, 'instance' => $instance, 'time' => time() + $t['time'], 'text' => $t['text']]);
                    $debug[] = "Добавлен таймер: " . var_export([$t['text'], $result], true);
                }
            }

            if(isset($auto_vars[$v['body']])) {
                $vars = $auto_vars[$v['body']];

                // если установлен вебхук - отправляем его
                if(isset($vars['hook']) && strlen($vars['hook']) > 3) {
                    $hookans = @file_get_contents(
                        $hook = str_replace('{num}', $clinum, $vars['hook'])
                    );

                    $debug[] = "Добавлен вебхук: {$hook}, ответ: {$hookans}";
                }
            }
            else
                $vars = [];

            if(isset($auto_flags[$v['body']])) {
                $flags = $auto_flags[$v['body']];

                // если стоит флаг "обратный звонок" - отправляем его
                if(isset($flags['call']) && $flags['call'] == true) {
                    $bitrix = &Mods::$bitrix;

                    $r = $bitrix->queryForever('voximplant.callback.start', [
                        'FROM_LINE' => $line = 'reg26482', // @todo настройка линии
                        'TO_NUMBER' => $clinum,
                        'TEXT_TO_PRONOUNCE' => $v['body']
                    ]);

                    $debug[] = "Добавлен обратный звонок ({$clinum}), линия {$line}, ID {$r['result']}";
                }

                // если стоит флаг "создать лид" - создаём
                if(isset($flags['lead']) && $flags['lead'] == true) {
                    $bitrix = &Mods::$bitrix;

                    // распределяем рандомному менеджеру
                    try {
                        $mgr = $this->getRandomManager();
                    } catch(\Exception $e) {
                        $mgr = 0;
                    }

                    // если переменная "заголовок лида" установлена - используем её
                    if(isset($vars['lead_title']) && strlen($vars['lead_title']))
                        $ltitl =  $vars['lead_title'];
                    else // иначе пихаем вместо этого скучную стандартную
                        $ltitl = "Входящий WhatsApp " . $clinum . " ~ " . $v['senderName'];

                    try {
                        // отправляем бессмертный (обход лимита на запросы) запрос Битриксу
                        $r = $bitrix->queryForever('crm.lead.add', $leadinfo = ['fields' => [
                            "TITLE" => $ltitl,
                            "NAME" => "",
                            "SECOND_NAME" => "",
                            "LAST_NAME" => "",
                            // если статус определён в переменной "стадия" - используем его, иначе NEW
                            "STATUS_ID" => isset($vars['lead_stage']) && strlen($vars['lead_stage']) ? $vars['lead_stage'] : "NEW",
                            "OPENED" => "Y",
                            "ASSIGNED_BY_ID" => $mgr,
                            "PHONE" => [["VALUE" => $clinum, "VALUE_TYPE" => "WORK"]]
                        ]]);

                        // немного устаревшая, но используемая фича
                        // закрепляем лид за чатом, чтобы потом видеть их связь
                        if (is_array($r) && isset($r['result'])) {
                            $this->db->findOne('bitrix_connection', ['chat' => $clinum, 'lead' => $r['result']]);
                            $lead_id_new = $r['result'];
                        } else // если не получилось создать лид - грустим и оповещаем
                            $lead_id_new = 'ошибка: ' . var_export($r, true);
                    } catch(\Exception $e) {
                        // если совсем всё сломалось - тоже оповещаем
                        $lead_id_new = 'ошибка: ' . $e;
                    }

                    // а если ничего не ломалось - радуем!
                    $debug[] = "Добавлен лид ({$ltitl})\nID лида: {$lead_id_new}";
                }
            }

            // рассказываем, какие мы молодцы
            $debug[] = "Сообщение обработано успешно";

            // отправляем отладочную инфу в отладочный чат с тэгом #webhook
            $this->send_debug_message("#webhook " . implode(PHP_EOL, $debug));

            if($ans == null) // если автоответа не нашлось совсем - просто выходим из функции
                return true;
            elseif(strpos($ans, '<--->') === false) // если нет символа <---> - отправляем одно сообщение
                $api->sendPhoneMessage('+' . $clinum, $ans);
            else // иначе разбиваем на несколько сообщений
                foreach(explode('<--->', $ans) as $mess)
                    $api->sendPhoneMessage('+' . $clinum, $mess);

            return true; // радостно выходим из функции
        }
    }

    /**
     * Отправляем отладочную инфу в Teleram-чат логов
     * @param $text
     * @param string $chat
     * @todo Асинхронность / вне общего потока
     * @return bool|string
     */
    public function send_debug_message($text, string $chat = 'astro')
    {
        switch($chat) {
            case 'astro':
                $chat = -376727758;
                break;

            case 'centrus':
                $chat = -379028438;
                break;

            case 'dev':
                $chat = 147581904;
                break;

            default:
                return false;
        }

        $bot_token = '765916505:AAGP-zhfKBbnsbpKZIZ8J1UyNS-sT8w_Wbg';

        $curl = \curl_init();
        \curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://api.telegram.org/bot'.$bot_token.'/sendMessage'
        ]);

        \curl_setopt($curl, CURLOPT_POST, 1);
        \curl_setopt($curl, CURLOPT_POSTFIELDS, 'chat_id='.$chat.'&text='.$text.'&parse_mode=markdown');
        \curl_setopt ($curl , CURLOPT_USERAGENT , "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-RU; rv:1.7.12) Gecko/20050919 Firefox/1.0.7");
        \curl_setopt ($curl, CURLOPT_RETURNTRANSFER, TRUE);
        \curl_setopt ($curl, CURLOPT_FAILONERROR, true);
        \curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);

        $return = \curl_exec($curl);
        \curl_close($curl);
        return $return;
    }

    /**
     * Получить сообщения по номеру чата
     * @param $author
     * @return array
     */
    public function getChatMessages($author)
    {
        return $this->db->find('messages', ['instance' => (string)$this->instance, '$or' => [['chatId' => $author], ['chatId' => new \MongoDB\BSON\Regex($author)]]])->toArray();
    }

    /**
     * Проверить, есть ли в базе сообщение по ID
     * @param string $id
     * @return bool
     */
    public function check(string $id)
    {
        $res = $this->db->findOne('messages', ['id' => $id, 'instance' => (string)$this->instance]);

        if($res instanceof \MongoDB\Model\BSONDocument && $res->id)
            return true;

        return false;
    }

    /**
     * Вставить сообщение в базу
     * @param array $array
     * @return mixed
     */
    public function insert(array $array)
    {
        return $this->db->insert('messages', $array);
    }
}