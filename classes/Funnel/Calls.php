<?php
namespace Astro\Funnel;

/**
 * Class Calls
 * @package Astro\Funnel
 */
class Calls extends Base
{
    /**
     * @var array
     */
    protected $criterias = [];

    /**
     * Sms constructor.
     * @param $db
     * @param string $from
     * @param string $to
     * @throws \Exception
     */
    public function __construct($db, string $from = '', string $to = '')
    {
        $this->criterias = ['SOURCE_ID' => new \MongoDB\BSON\Regex('CALL')];
        parent::__construct($db, $from, $to);
    }
}