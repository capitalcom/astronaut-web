<?php
namespace Astro\Funnel;

/**
 * Class Whatsapp
 * @package Astro\Funnel
 */
class Whatsapp extends Base
{
    /**
     * @var array
     */
    protected $criterias = ['UTM_CAMPAIGN' => ['$in' => ['whatsapp', 'whatsapp-rassylka', 'whatsapp-bot']]];
}