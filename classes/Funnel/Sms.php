<?php
namespace Astro\Funnel;

/**
 * Class Sms
 * @package Astro\Funnel
 */
class Sms extends Base
{
    /**
     * @var array
     */
    protected $criterias = [];

    /**
     * Sms constructor.
     * @param $db
     * @param string $from
     * @param string $to
     * @throws \Exception
     */
    public function __construct($db, string $from = '', string $to = '')
    {
        $this->criterias = ['UTM_CAMPAIGN' => new \MongoDB\BSON\Regex('sms')];
        parent::__construct($db, $from, $to);
    }
}