<?php
    namespace Astro\Funnel;

    /**
     * Class Youtube
     * @package Astro\Funnel
     */
    class Youtube extends Base
    {
        /**
         * @var array
         */
        protected $criterias = ['UTM_CAMPAIGN' => ['$in' => ['yutub', 'yutub', 'youtube']]];
    }