<?php
namespace Astro\Funnel;

/**
 * Class Instagram
 * @package Astro\Funnel
 */
class Instagram extends Base
{
    /**
     * @var array
     */
    protected $criterias = ['UTM_SOURCE' => ['$in' => ['instagram', 'insta', 'inst']]];
}