<?php
namespace Astro\Funnel;

/**
 * Class Telegram
 * @package Astro\Funnel
 */
class Telegram extends Base
{
    /**
     * @var array
     */
    protected $criterias = ['UTM_SOURCE' => 'telegram'];
}