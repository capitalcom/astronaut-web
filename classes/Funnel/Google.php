<?php
namespace Astro\Funnel;

/**
 * Class Google
 * @package Astro\Funnel
 */
class Google extends Base
{
    /**
     * @var array
     */
    protected $criterias = ['UTM_SOURCE' => 'google'];
}