<?php
namespace Astro\Funnel;

/**
 * Class Facebook
 * @package Astro\Funnel
 */
class Facebook extends Base
{
    /**
     * @var array
     */
    protected $criterias = ['UTM_SOURCE' => 'facebook'];
}