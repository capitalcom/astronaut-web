<?php
namespace Astro\Funnel;

/**
 * Class VK
 * @package Astro\Funnel
 */
class VK extends Base
{
    /**
     * @var array
     */
    protected $criterias = ['UTM_SOURCE' => 'vk'];
}