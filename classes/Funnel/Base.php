<?php

namespace Astro\Funnel;

/**
 * Class Base
 * @package Astro\Funnel
 */
class Base
{
    /**
     * @var \Astro\DB
     */
    protected $db;

    /**
     * @var array
     */
    public static $cols = ['Охват', 'Клики', 'Лиды', 'Продажи', 'Выручка'];

    /**
     * @var array
     */
    public static $funcs = ['getHits', 'getClicks', 'getLeads', 'getSells', 'getPlus'];

    /**
     * @var array
     */
    public static $rows = [
        'Google' => 'Google',
        'Facebook' => 'Facebook',
        'YouTube' => 'Youtube',
        'Instagram' => 'Instagram',
        # 'Facebook Ads' => 'Facebookads',
        'ВКонтакте' => 'VK',
        'SMS' => 'Sms',
        'Telegram' => 'Telegram',
        # 'Переходы с внутренних сайтов' => ,
        'Вх. звонки' => 'Calls',
        # 'Сарафан' => null,
        'Переходы с поисковых систем' => 'Search',
        'WhatsApp' => 'Whatsapp',
        # 'Прямые переходы' => 'Direct',
        # 'Другие источники' => 'Other'
    ];

    /**
     * @var array
     */
    protected $criterias = [];

    /**
     * Base constructor.
     * @param $db
     * @param string $from
     * @param string $to
     * @throws \Exception
     */
    public function __construct(&$db, string $from = '', string $to = '') {
        if(!strlen($from))
            $from = '-1 week';
        if(!strlen($to))
            $to = 'now';

        $this->criterias['DATE_CREATE'] = [
            '$lte' => (new \DateTime($to))->getTimestamp(),
            '$gte' => (new \DateTime($from))->getTimestamp(),
        ];

        $this->db = $db;
    }

    public function getHits() {
        return 0;
    }

    public function getClicks() {
        return 0;
    }

    /**
     * @return int
     */
    public function getLeads() {
        return $this->softCount('leads');
    }

    /**
     * @return int
     */
    public function getSells() {
        return $this->softCount('deals', ['STAGE_ID' => 'WON']);
    }

    /**
     * @return int
     */
    public function getPlus() {
        $plus = 0;

        foreach($this->db->find('deals', array_merge(['STAGE_ID' => 'WON'], $this->criterias)) as $deal) {
            if(isset($deal->OPPORTUNITY) && is_numeric($deal->OPPORTUNITY) && $deal->OPPORTUNITY > 0)
                $plus += $deal->OPPORTUNITY;
        }

        return $plus;
    }

    /**
     * @param string $table
     * @param array $criterias
     * @return int
     */
    protected function softCount(string $table, array $criterias = []) {
        return $this->db->count($table, array_merge($criterias, $this->criterias));
    }
}