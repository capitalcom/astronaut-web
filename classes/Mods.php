<?php

namespace Astro;

/**
 * Служебный класс для хранения экземпляров других классов текущего сайта, а также их подготовки
 * @package Astro
 */
class Mods
{
    /**
     * @var DB
     */
    public static $db;

    /**
     * @var DB
     */
    public static $adb;

    /**
     * @var \Redis
     */
    public static $redis;

    /**
     * @var ChatApi
     */
    public static $api;

    /**
     * @var Bitrix
     */
    public static $bitrix;

    /**
     * @var Messages
     */
    public static $messages;

    /**
     * @var Multisite
     */
    public static $ms;

    /**
     * Готовит "почву" для текущего поддомена
     *
     * $db - доступ к базе данных текущего сайта (поддомена)
     * $adb - общая для всех поддоменов база с базовой информацией
     * $redis - экземпляр Redis-подключения
     * $bitrix - экземпляр Битрикс-класса
     * $api - экземпляр Chat-API класса
     * $messages - экземпляр Messages класса
     * @see \Astro\Messages
     * @see \Astro\DB
     * @see \Astro\Multisite
     * @see \Astro\Bitrix
     * @see \Astro\ChatApi
     * @param int $instance
     * @throws \Exception
     */
    public static function prepare(int $instance = null)
    {
        self::$db = new \Astro\DB(_DB_CONNECTION, _DB_BASE);
        self::$adb = new \Astro\DB(_DB_CONNECTION, 'astro_common');
        self::$redis = new \Redis();
        self::$redis->connect(_REDIS_URL, 6379);
        self::$redis->auth(_REDIS_PASS);

        $ms = new \Astro\Multisite(ASTRO_DOMAIN_NAME, $instance);
        self::$ms = &$ms;

        if(!is_numeric($instance))
            $instance = $ms->getIID();

        if(!is_array($classes = $ms->getClasses()))
            throw new \Exception('Не удаётся получить доступ к внутренним инструментам');

        self::$bitrix = &$classes['bitrix'];
        self::$api = &$classes['api'];
        self::$messages = new \Astro\Messages($instance);
    }
}