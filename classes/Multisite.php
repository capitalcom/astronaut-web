<?php
    namespace Astro;

    /**
     * Класс для управления системой мультисайтов. Помогает определять, добавлять и изменять мультисайты
     * @package Astro
     */
    class Multisite
    {
        /**
         * @var string
         */
        protected $url;

        /**
         * @var int|null
         */
        protected $instance = null;

        /**
         * Список всех сайтов и данных о них. Временный вариант
         * Формат:
         *  'URL сайта' => ['chat-api instance', 'bitrix webhook url', 'bitrix token']
         * @todo Переместить всё в базу и админку
         * @var array
         */
        public $sites = [
            'admin.astronaut.kz' => ['12736', '', '', []],
            'bb.astronaut.kz' => ['12736', 'https://biznesbastau2018.bitrix24.kz/rest/4113/', 'zezdr35wzqangapu', [
                12736, 17113, 17114, 17115, 17116, 17117, 17649, 17650, 17651, 17652, 17653,
                18858, 18859, 21192, 21193
            ]],
            '1.astronaut.kz' => ['18851', 'https://b24-l403mc.bitrix24.kz/rest/1/', '9sptx4bc2y5sd5pg', []],
            '2.astronaut.kz' => ['18852', 'https://b24-0pdq1q.bitrix24.kz/rest/1/', 'hoto5nxbkxi34pua', []],
            '3.astronaut.kz' => ['18854', 'https://b24-qf3f7r.bitrix24.kz/rest/1/', 'pylmn96658o34hgq', []],
            '4.astronaut.kz' => ['18855', 'https://b24-14lxed.bitrix24.kz/rest/1/', 'crslgq6ijo78jt0u', []],
            '5.astronaut.kz' => ['18856', 'https://b24-vn7p5o.bitrix24.kz/rest/1/', 'kmqk5f1vepwnrzpl', []],
            '6.astronaut.kz' => ['18859', 'https://b24-382d9s.bitrix24.kz/rest/1/', 'dk0vichi1eifc2pl', []]
        ];

        /**
         * Multisite constructor.
         * @param string $url
         * @param int|null $instance
         */
        public function __construct(string $url = '', int $instance = null)
        {
            $this->instance = $instance;

            if(strlen($url))
                $this->url = $url;
            elseif(php_sapi_name() === 'cli')
                $this->url = 'admin.astronaut.kz';
            elseif(defined('ASTRO_DOMAIN_NAME'))
                $this->url = ASTRO_DOMAIN_NAME;
            else
                $this->url = $_SERVER['HTTP_HOST'];
        }

        /**
         * Получить домен по Chat-API инстансу
         * Функция возвращает именно тот домен, к которому данный инстанс прикреплён
         * @param int $instance
         * @return string|null
         */
        public function getDomainByInstance(int $instance)
        {
            foreach($this->sites as $domain => $inf) {
                if(
                    ($domain != 'admin.astronaut.kz' && $inf[0] == $instance)
                    ||
                    in_array((int)$instance, $inf[3])
                    ||
                    in_array((string)$instance, $inf[3])
                )
                    return $domain;
            }

            return null;
        }

        /**
         * Получить домен текущего экземляра класса
         * @return string
         */
        public function getDomain(): string
        {
            return $this->url;
        }

        /**
         * @return array
         */
        public function getMyIIDs(): array
        {
            return $this->detect()[3];
        }

        /**
         * Проверить, подключён ли сайт по домену
         * @return bool
         */
        public function check(): bool
        {
            return isset($this->sites[$this->url]);
        }

        /**
         * Получить информацию по домену
         * @return bool|mixed
         */
        public function detect()
        {
            return isset($this->sites[$this->url]) ? $this->sites[$this->url] : false;
        }

        /**
         * Получить инстанс-ID выбранного домена
         * @return mixed
         */
        public function getIID(): int
        {
            if(is_numeric($this->instance))
                return $this->instance;

            return $this->detect()[0];
        }

        /**
         * Получить набор служебных классов текущего домена
         * @param null $db
         * @see \Astro\Mods::prepare()
         * @return array|bool
         */
        public function getClasses($db = null)
        {
            $d = $this->detect();

            if(!is_array($d))
                return false;

            $iid = is_numeric($this->instance) ? $this->instance : $d[0];

            $inst = Mods::$adb->findOne('instance', ['id' => (string)$iid]);

            if(isset($inst->token))
                $api = new ChatApi($inst->token, $inst->url);

            $bitrix = new Bitrix($d[2], $d[1], $db);
            return ['api' => $api, 'bitrix' => $bitrix];
        }

        /**
         * @return ChatApi|bool
         */
        public function getChatApi()
        {
            $d = $this->detect();

            if(!is_array($d))
                return false;

            $iid = is_numeric($this->instance) ? $this->instance : $d[0];

            $inst = Mods::$adb->findOne('instance', ['id' => $iid]);

            if(isset($inst->token))
                return new ChatApi($inst->token, $inst->url);
            else
                return false;
        }

        /**
         * @return Bitrix|bool
         */
        public function getBitrix()
        {
            $d = $this->detect();

            if(!is_array($d))
                return false;

            return new Bitrix($d[2], $d[1]);
        }
    }