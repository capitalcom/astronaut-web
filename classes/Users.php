<?php

namespace Astro;

/**
 * Класс для работы с локальными пользователями
 * @package Astro
 */
class Users
{
    /**
     * @var DB|null
     */
    protected $db = null;

    /**
     * @var \MongoDB\BSON\ObjectId|null
     */
    protected $id = null;

    /**
     * Users constructor.
     * @param \MongoDB\BSON\ObjectId $id
     */
    public function __construct(\MongoDB\BSON\ObjectId $id)
    {
        $this->db = &Mods::$db;
        $this->id = $id;
    }

    /**
     * Проверить, существует ли выбранный пользователь
     * @return bool
     */
    public function check()
    {
        $res = $this->db->findOne('users', ['_id' => $this->id]);

        if(!is_array($res) && !is_object($res)) {
            return false;
        }

        return true;
    }

    /**
     * Установить поле в БД
     * @param $field
     * @param $value
     * @return int|null
     */
    public function __set($field, $value)
    {
        return $this->db->update('users', ['_id' => $this->id], [$field => $value]);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return ($this->login === 'admin' || (isset($this->admin) && $this->admin == 1));
    }

    /**
     * Получить объект пользователя
     * @return array|object|null
     */
    public function get()
    {
        return $this->db->findOne('users', ['_id' => $this->id]);
    }

    /**
     * Получить поле пользователя
     * @param $field
     * @return mixed|null
     */
    public function __get($field)
    {
        $res = $this->db->findOne('users', ['_id' => $this->id]);

        if(!is_object($res) || is_null($res->{$field}))
            return null;
        else
            return $res->{$field};
    }

    /**
     * Зарегистрировать нового пользователя
     * @param string $login
     * @param string $password
     * @return mixed
     */
    public static function create(string $login, string $password)
    {
        return (Mods::$db)->insert('users', [
            'login' => $login,
            'password' => md5($password)
        ]);
    }

    /**
     * @return bool|int
     */
    public function getTeamInstance()
    {
        if(!is_string($this->team) || strlen($this->team) < 1 || $this->team === 'команда...')
            return false;

        $res = (Mods::$db)->findOne('teams', []);

        if(!is_object($res))
            return false;

        if(!isset($res->instance) || !is_numeric($res->instance))
            return false;

        return $res->instance;
    }

    /**
     * @return array|bool|object|null
     */
    public static function sevenDevils()
    {
        $res = (Mods::$db)->findOne('users', []);

        if(!is_array($res) && !is_object($res)) {
            return false;
        }

        return $res;
    }

    /**
     * Найти пользователя по логину и паролю
     * @param $login
     * @param $password
     * @return array|bool|null|object
     */
    public static function login($login, $password)
    {
        $res = (Mods::$db)->findOne('users', [
            'login' => $login,
            'password' => md5($password)
        ]);

        if(!is_array($res) && !is_object($res)) {
            return false;
        }

        return $res;
    }
}