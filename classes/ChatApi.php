<?php
    namespace Astro;

    /**
     * Middleware между chat-api.com и нами
     * @package Astro
     * @author Mike Chip
     */
    class ChatApi
    {
        protected $token = '';
        protected $url = '';
        protected $instance_url = 'https://us-central1-app-chat-api-com.cloudfunctions.net/';
        protected $instance_key = '9AuVEaEwgeSu3eMCTceScwdyp3l2';
        protected $_mem = [];

        /**
         * Подготовить экземпляр
         * @param string $token
         * @param string $url
         * @return void
         */
        public function __construct($token, $url = 'https://foo.chat-api.com')
        {
            $this->token = $token;
            $this->url = $url;
        }

        /**
         * Сгенерировать URL для запроса
         * @param $method
         * @param array $args
         * @return string
         */
        public function createUrl($method, $args = [])
        {
            $args['token'] = $this->token;
            return $this->url.'/'.$method.'?'.http_build_query($args);
        }

        /**
         * Отправить API-запрос
         * @param string $method - метод API
         * @param null|array $args - аргументы
         * @param string $qmethod - метод HTTP (GET/POST)
         * @return bool|string
         */
        public function query($method, $args = null, $qmethod = 'GET')
        {
            $url = $this->createUrl($method);

            if($qmethod == "POST" && isset($args) && is_array($args)) {
                $json = json_encode($args);

                $options = stream_context_create(['http' => [
                    'method' => $qmethod,
                    'header' => 'Content-type: application/json',
                    'content' => $json
                ]]);
            } elseif($qmethod == "GET" && isset($args) && is_array($args)) {
                $url = $this->createUrl($method, $args);

                $options = stream_context_create(['http' => [
                    'method' => $qmethod,
                    'header' => 'Content-type: application/json',
                ]]);
            }

            return @file_get_contents($url, false, isset($options) ? $options : null);
        }

        /**
         * Рекурсивно получить полный список сообщений
         * Не использовать без острой нужды! Это будет длиться вечность
         * @return array|mixed|null
         */
        public function getFullInbox()
        {
            $inbox = [];

            while(true) {
                $inb = json_decode($this->query('messages', isset($offset) ? ['lastMessageNumber' => $offset] : ['last' => 1], "GET"), true);

                if(!is_array($inb) || !isset($inb['messages']) || !count($inb['messages'])) {
                    var_dump($inb);
                    break;
                }

                $offset = $inb['lastMessageNumber'];
                $inbox = array_merge($inbox, $inb['messages']);
            }

            return $inbox;
        }

        /**
         * Создать новый инстанс по Instance API
         * @return false|string
         */
        public function createInstance()
        {
            $options = stream_context_create(['http' => [
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => json_encode(['uid' => $this->instance_key, 'type' => 'whatsapp'])
            ]]);

            return json_decode(file_get_contents($this->instance_url.'newInstance', false, isset($options) ? $options : null), true);
        }

        /**
         * Удалить инстанс
         * @param int $id
         * @return mixed
         */
        public function deleteInstance(int $id)
        {
            $options = stream_context_create(['http' => [
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => json_encode(['uid' => $this->instance_key, 'instanceId' => (string)$id])
            ]]);

            return json_decode(file_get_contents($this->instance_url.'deleteInstance', false, isset($options) ? $options : null), true);
        }

        /**
         * Получить последние сообщения по API (+ очередь сообщений)
         * @return null|array
         */
        public function getInbox($offset = 0)
        {
            if(isset($this->_mem[$offset]) && is_array($this->_mem[$offset]))
                return $this->_mem[$offset];

            $inbox = json_decode($this->query('messages', ($offset > 0) ? ['lastMessageNumber' => $offset] : ['last' => 1]), 1);

            $newOffset = $inbox['lastMessageNumber'];
            $mess = $inbox['messages'];
            $inbox = [];

            foreach($mess as $val) {
                $val['offset'] = $newOffset;
                $inbox[] = $val;
            }

            if($offset < 50) {
                $que = json_decode($this->query('showMessagesQueue'), 1)['first100'];

                if (is_array($que)) {
                    foreach ($que as $k => $v) {
                        $inbox[] = [
                            'id' => $v['id'],
                            'body' => $v['body'],
                            'type' => $v['type'],
                            'senderName' => '',
                            'fromMe' => true,
                            'queue' => true,
                            'author' => $v['chatId'],
                            'time' => $v['last_try'],
                            'chatId' => $v['chatId']
                        ];
                    }
                }
            }

            usort($inbox, function ($a, $b) {
                if ($a['time'] == $b['time'])
                    return 0;
                return ($a['time'] < $b['time']) ? -1 : 1;
            });

            $this->_mem[$offset] = $inbox;
            return $inbox;
        }

        /**
         * Получить статус инстанса
         * @return string
         */
        public function getStatus()
        {
            $js = json_decode($this->query('status'), 1);

            if(isset($js['accountStatus']))
                return $js['accountStatus'];
            else
                return 'error';
        }

        /**
         * Получить сообщения по чату
         * @param string $author
         * @return array
         */
        public function getChatMessages($author)
        {
            $ib = $this->getInbox();
            $msgs = [];

            foreach($ib as $message) {
                if(isset($author) && $author == $message['chatId'])
                    $msgs[] = $message;
            }

            return $msgs;
        }

        /**
         * Отправить сообщение по номеру телефона
         * @param string $chat
         * @param string $text
         * @return boolean
         */
        public function sendPhoneMessage($chat, $text)
        {
            return json_decode($this->query('sendMessage', ['phone' => $chat, 'body' => $text]), 1)['sent'];
        }

        /**
         * Получить текущий вебхук
         * @return null|array
         */
        public function getWebhook()
        {
            return json_decode($this->query('webhook', []));
        }

        /**
         * Очистить очередь сообщений. Используйте только когда всё совсем плохо
         * @return mixed
         */
        public function clearQueue()
        {
            return json_decode($this->query('clearMessagesQueue', []));
        }

        /**
         * Установить новый вебхук. Осторожно: неправильным хуком можно всё сломать
         * Генератор вебхуков находится в админке
         * @return null|array
         */
        public function setWebhook($url = '')
        {
            if(empty($url))
                $url = 'http://'.ASTRO_DOMAIN_NAME.'/webhook.php';

            return json_decode($this->query('webhook', ['webhookUrl' => $url]));
        }


        /**
         * Разлогиниться
         * @return null|array
         */
        public function logout()
        {
            return json_decode($this->query('logout', []));
        }

        /**
         * Перезагрузить инстанс
         * @return null|array
         */
        public function reboot()
        {
            return json_decode($this->query('reboot', []));
        }

        /**
         * Получить прямую ссылку на QR-код.
         * @todo Сделать прокси, чтобы пользователь не мог узнать данные об инстансе (юрл и токен)
         * @return string
         */
        public function getQRCode()
        {
            return $this->createUrl('qr_code');
        }

        /**
         * Получить прямую ссылку на скриншот
         * @todo Сделать прокси, чтобы пользователь не мог узнать данные об инстансе (юрл и токен)
         * @return string
         */
        public function getScreenshot()
        {
            return $this->createUrl('screenshot');
        }

        /**
         * Отправить файл в чат
         * @param string $chat
         * @param string $body
         * @param string $filename
         * @return boolean
         */
        public function sendFile($chat, $body, $filename)
        {
            return json_decode($this->query('sendFile', ['chatId' => $chat, 'filename' => $filename, 'body' => $body], 'POST'), 1)['sent'];
        }

        /**
         * Отправить сообщение по номеру чата (не телефона, здесь нужно нечто вроде 7123123123@c.us)
         * @param string $chat
         * @param string $text
         * @return boolean
         */
        public function sendMessage($chat, $text)
        {
            return json_decode($this->query('sendMessage', ['chatId' => $chat, 'body' => $text]), 1)['sent'];
        }

        /**
         * Генерирует список диалогов на основе сообщений
         * @param int $offset
         * @deprecated
         * @see \Astro\Messages::updateDialogs()
         * @return array
         */
        public function getDialogs($offset = 0)
        {
            $ib = $this->getInbox($offset);
            $contacts = [];

            foreach($ib as $message) {
                $contacts[ $message['chatId'] ] = [
                    'name' => $message['senderName'],
                    'answered' => $message['fromMe'],
                    'offset' => isset($message['offset']) ? $message['offset'] : 0,
                    'last' => mb_substr($message['body'], 0, 50),
                    'lastFull' => $message['body'],
                    'chatId' => $message['chatId'],
                    'num' => '+' . (int)filter_var($message['chatId'], FILTER_SANITIZE_NUMBER_INT)
                ];
            }

            return $contacts;
        }
    }