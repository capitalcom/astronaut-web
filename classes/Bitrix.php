<?php

namespace Astro;

/**
 * Middleware-прокладка для работы с Bitrix24
 * @package Astro
 */
class Bitrix
{
    protected $token;
    protected $api_url;
    protected $db;

    /**
     * Передайте токен и URL для работы с Rest API
     * Их можно сгенерировать в меню Приложения -> Вебхуки -> Входящие
     * @param string $token
     * @param string $url
     * @param null $db
     */
    public function __construct(string $token = '1j4ww2lf3figdr4g', string $url = 'https://biznesbastau2018.bitrix24.kz/rest/4075/', $db = null)
    {
        $this->token = $token;
        $this->api_url = $url;

        if(is_null($db))
            $db = &Mods::$db;

        $this->db = $db;
    }

    /**
     * Обновить локальную базу данных лидов
     * @return bool
     * @throws \Exception
     */
    public function leadsUpdate()
    {
        $list = $this->getList('crm.lead.list', ['select' => ["*", "UF_*"], 'filter' => ['>DATE_MODIFY' => new \DateTime('-1 hour')]]);
        $leads = 0;

        foreach($list['result'] as $el) {
            if(count($this->db->find('leads', ['ID' => $el['ID']])->toArray()) > 0)
                continue;

            if(isset($el['DATE_CREATE']))
                $el['DATE_CREATE'] = strtotime($el['DATE_CREATE']);

            if(isset($el['DATE_MODIFY']))
                $el['DATE_MODIFY'] = strtotime($el['DATE_MODIFY']);

            $this->db->insert('leads', $el);
            $leads++;
        }

        return $leads;
    }

    /**
     * Обновить локальную базу данных звонков
     * @return bool
     * @throws \Exception
     */
    public function callsUpdate()
    {
        $list = $this->getList('voximplant.statistic.get', ['filter' => ['>CALL_START_DATE' => (new \DateTime('-1 hour'))->format('Y-m-d\TH:m:i')]]);

        $calls = 0;
        foreach($list['result'] as $el) {
            if(count($this->db->find('calls', ['ID' => $el['ID']])->toArray()) > 0)
                continue;

            if(isset($el['CALL_START_DATE']))
                $el['CALL_START_DATE'] = $el['DATE_CREATE'] = strtotime($el['CALL_START_DATE']);

            $this->db->insert('calls', $el);
            $calls++;
        }

        return $calls;
    }

    /**
     * @return int
     */
    public function callsHistoryUpdate()
    {
        $cnt = 0;

        $this->getList('voximplant.statistic.get', [], function($data) {
            global $cnt;

            foreach($data as $el) {
                $cnt++;

                if(count($this->db->find('calls', ['ID' => $el['ID']])->toArray()) > 0) {
                    echo "Call " . $el['ID'] . " - IGNORED" . PHP_EOL;
                    continue;
                }

                if(isset($el['CALL_START_DATE']))
                    $el['CALL_START_DATE'] = $el['DATE_CREATE'] = strtotime($el['CALL_START_DATE']);

                echo "Call " . $el['ID'] . " inserted" . PHP_EOL;
                $this->db->insert('calls', $el);

            }
        });

        return $cnt;
    }

    /**
     * @return int
     */
    public function parseContacts()
    {
        $cnt = 0;

        $this->getList('crm.contact.list', [], function($data) {
            global $cnt;

            foreach($data as $el) {
                $cnt++;

                if(count($this->db->find('contacts', ['ID' => $el['ID']])->toArray()) > 0) {
                    echo "Contact " . $el['ID'] . " - IGNORED" . PHP_EOL;
                    continue;
                }

                echo "Contact " . $el['ID'] . " inserted" . PHP_EOL;
                $this->db->insert('contacts', $el);

            }
        });

        return $cnt;
    }

    /**
     * Обновить локальную базу данных сделок
     * @return bool
     * @throws \Exception
     */
    public function dealsUpdate()
    {
        $list = $this->getList('crm.deal.list', ['select' => ["*", "UF_*"], 'filter' => ['>DATE_MODIFY' => new \DateTime('-1 hour')]]);

        $deals = 0;
        foreach($list['result'] as $el) {
            if(isset($el['DATE_CREATE']))
                $el['DATE_CREATE'] = strtotime($el['DATE_CREATE']);

            if(isset($el['DATE_MODIFY']))
                $el['DATE_MODIFY'] = strtotime($el['DATE_MODIFY']);

            if(count($this->db->find('deals', ['ID' => $el['ID']])->toArray()) > 0)
                $this->db->update('deals', ['ID' => $el['ID']], $el);
            else {
                $this->db->insert('deals', $el);
                $deals++;
            }
        }

        return $deals;
    }

    /**
     * @param $method
     * @param array $params
     * @param string|null $token
     * @return mixed
     */
    public function query($method, array $params = [], string $token = null)
    {
        if(is_null($token))
            $token = $this->token;

        return $this->curl($this->api_url.$token.'/'.$method.'/', $params);
    }

    /**
     * Отправлять запрос до тех пор, пока не будет получен ответ
     * Нужно для обхода лимита на запросы.
     * Осторожно: он будет долбить до второго пришествия. Установите временной лимит
     *
     * @param $method
     * @param array $params
     * @param int $tries
     * @return mixed|null
     */
    public function queryForever($method, array $params = [], int $tries = 0)
    {
        if($tries >= 20)
            return null;

        $list = $this->query($method, $params);

        if(isset($list['error'])) {
            sleep(2);
            return $this->queryForever($method, $params, $tries+1);
        }

        return $list;
    }

    /**
     * Обновить локальную базу данных менеджеров
     * @return int
     */
    public function managersUpdate()
    {
        $users = $this->getList('user.get', ['select' => ['*', 'UF_*']]);
        $users_cnt = 0;
        $departs = [5, 99, 101, 103, 115, 113];

        if(!isset($users['result']) | !count($users['result']))
            return 0;

        $this->db->delete('managers', []);

        foreach($users['result'] as $key => $info) {
            if (!isset($info['UF_DEPARTMENT']))
                continue;

            if (!isset($info['LAST_NAME']) || $info['NAME'] != $info['LAST_NAME'])
                continue;

            foreach ($info['UF_DEPARTMENT'] as $did) {
                if (in_array($did, $departs)) {
                    $this->db->upsert('managers', ['ID' => $info['ID']], $info);
                    $users_cnt++;
                    break;
                }
            }
        }

        return $users_cnt;
    }

    /**
     * Получить список нужных сущностей (напр. лидов) и применить к ним callback
     * @param $method
     * @param array $params
     * @param callable $func
     * @return bool
     */
    public function getCallback($method, array $params, callable $func)
    {
        $list = $this->query($method, $params);

        if(isset($list['error'])) {
            $func(false);
            sleep(2);
            return $this->getCallback($method, $params, $func);
        }

        if(!is_array($list) || !isset($list['result']) || !count($list['result']))
            return false;

        foreach($list['result'] as $res) {
            $func($res);
        }

        if(isset($list['next'])) {
            $params['start'] = $list['next'];

            if(is_array($child = $this->getCallback($method, $params, $func)) && isset($child['result']) && count($child['result']))
                $func($child['result']);
        }

        return true;
    }

    /**
     * Отправить API-запрос и получить массив-список
     * Используется только для методов crm.*.list
     * @param $method
     * @param array $params
     * @param callable $cb
     * @return array|mixed
     */
    public function getList($method, array $params, callable $cb = null)
    {
        $list = $this->queryForever($method, $params);

        if(!is_array($list) || !isset($list['result']) || !count($list['result']))
            return [];

        if(is_callable($cb))
            $cb($list['result']);

        if(isset($list['next'])) {
            $params['start'] = $list['next'];

            if(is_array($child = $this->getList($method, $params, $cb)) && count($child['result'])) {
                if(is_callable($cb))
                    $cb($child['result']);
                else
                    $list['result'] = array_merge($list['result'], $child['result']);
            }
        }

        return $list;
    }

    /**
     * Прокладка для CURL
     * @param $url
     * @param $params
     * @return mixed
     */
    protected function curl($url, $params)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => http_build_query($params),
        ]);

        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result, 1);
    }
}