<?php
    if(!isset($ms))
        $ms = new \Astro\Multisite();

    if(isset($_REQUEST['url'])) {
        $res = 'http://'.$_REQUEST['url'].'/webhook_'.$_REQUEST['id'].'.php';
    }
?>

<style>
    select {
        width: 100%;
    }
</style>

<?php if(isset($res)) { ?>
    <div class="alert alert-info">
        Ваш вебхук: <a target="_blank" href="<?=$res;?>"><strong><?=$res;?></strong></a>
    </div>
<?php } ?>

<h3>Генератор вебхуков</h3>
На этой странице можно сгенерировать URL для вебхука Chat-Api.
<hr>
<form action="" method="POST">
    <p>
        <select name="url">
            <option disabled="disabled">Выберите сайт</option>
            <?php
                foreach($ms->sites as $k => $v) {
                    if ($k == $_SERVER['HTTP_HOST'])
                        continue;

                    echo "<option value=\"{$k}\">{$k}</option>";
                }
            ?>
        </select>
    </p>
    <p>
        <select name="id">
            <option disabled="disabled">Выберите инстанс</option>
            <?php
                foreach((\Astro\Mods::$adb)->find('instance', []) as $item) {
                    echo "<option value=\"{$item->id}\">{$item->id}</option>";
                }
            ?>
        </select>
    </p>

    <input type="submit" value="Сгенерировать">
</form>