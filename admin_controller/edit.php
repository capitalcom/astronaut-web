<?php
    $db = &\Astro\Mods::$adb;

    if(isset($_REQUEST['set'])) {
        $upd = $db->update('instance', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])], ['set' => json_decode($_REQUEST['set'], true)]);
        ob_flush();
        die($upd);
    }

    if(isset($_REQUEST['setflags'])) {
        $upd = $db->update('instance', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])], ['flags' => json_decode($_REQUEST['setflags'], true)]);
        ob_flush();
        die($upd);
    }

    if(isset($_REQUEST['set'])) {
        $upd = $db->update('instance', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])], [$_REQUEST['key'] => $_REQUEST['value']]);
        ob_flush();
        die($upd);
    }

    if(isset($_REQUEST['setvars'])) {
        $upd = $db->update('instance', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])], ['vars' => json_decode($_REQUEST['setvars'], true)]);
        ob_flush();
        die($upd);
    }

    if(isset($_REQUEST['settimers'])) {
        $upd = $db->update('instance', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])], ['timers' => json_decode($_REQUEST['settimers'], true)]);
        ob_flush();
        die($upd);
    }

    // Ищем информацию об инстансе по общей базе
    $auto_inf = $db->findOne('instance', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])]);

    // Запоминаем Chat-Api ID ($id) и внутренний ID в базе данных ($_id)
    $id = $auto_inf->id;
    $_id = $_REQUEST['id'];

    // Если мы знаем токен инстанса - получаем дополнительную инфу
    if(is_object($auto_inf) && isset($auto_inf->token)) {
        $api = new \Astro\ChatApi($auto_inf->token, $auto_inf->url);

        if(isset($_REQUEST['restart'])) {
            $api->reboot();
            header('Location: ?');
            return;
        } elseif(isset($_REQUEST['relog'])) {
            $api->logout();
            header('Location: ?');
            return;
        }

        switch ($api->getStatus()) {
            case 'init':
                $status = 'Не инициализирован';
                break;

            case 'error':
                $status = 'Ожидайте загрузки';
                break;

            case 'loading':
                $status = 'Загрузка данных';
                break;

            case 'got qr code':
                $status = 'Получен QR-код';
                $showqr = 1;
                break;

            case 'authenticated':
                $status = 'Онлайн';
                break;
        }
    }
    // Ну а если нет - досвиданья

    // Получаем инфу об автоответчике и прочем, если нет - пустые массивы
    if(!is_object($auto_inf) || !isset($auto_inf->set)) {
        $auto_inf = [];
        $auto_flags = [];
        $auto_vars = [];
        $auto_timers = [];
    } else {
        $auto_flags = (isset($auto_inf->flags)) ? $auto_inf->flags : [];
        $auto_vars = isset($auto_inf->vars) ? $auto_inf->vars : [];
        $auto_timers = isset($auto_inf->timers) ? $auto_inf->timers : [];
        $auto_inf = $auto_inf->set;
    }

    /*
     * Изощрённым методом получаем статистику
     * @todo Переместить в какой-то класс
     */
    $ms = new \Astro\Multisite();
    $domain = $ms->getDomainByInstance((int)$id);

    if(is_string($domain)) {
        $showstats = true;
        $idb = new \Astro\DB(_DB_CONNECTION, _generate_db_name($ms->getDomainByInstance($id)));
        $today = ['instance' => $id, 'time' => ['$gte' => (new \DateTime('today'))->getTimestamp(), '$lte' => time()]];
        $messages = $idb->count('messages', ['instance' => $id]);
        $dialogs = $idb->count('dialogs', ['instance' => $id]);
        $messages2 = $idb->count('messages', $today);
        $dialogs2 = $idb->count('dialogs', $today);
    }
?>

<script>
    function _do_secret_query(el) {
        if(el.id === 'del') {
            if(!confirm('Вы уверены?'))
                return;
        }

        url = el.href;

        $.get(url, function(a) {
            console.log(a);
            location.reload();
        });

        return false;
    }
</script>

<h2>Instance #<?=$id;?></h2>
<table>
    <tr>
        <td>
            <a onclick="return _do_secret_query(this)" href="?restart" class="btn btn-warning"><i class="fa fa-pause"></i> Рестарт</a>
        </td>
        <td>
            <a onclick="return _do_secret_query(this)" href="?relog" class="btn btn-info"><i class="fa fa-user"></i> Переавторизация</a>
        </td>
        <td>
            <a href="#" data-toggle="modal" data-target="#screen-modal" class="btn btn-info"><i class="fa fa-photo"></i> Скриншот</a>
        </td>
        <td>
            <a onclick="return _do_secret_query(this)" id="del" href="?act=list&id=<?=$id;?>&delete" class="btn btn-danger">&times; Удалить</a>
        </td>
    </tr>
</table>
<hr>

<p>
    <b>Статус:</b> <?=$status;?>
</p>
<?php if(isset($showstats)) { ?>
<p>
    <b>Общее число сообщений:</b> <?=$messages;?> <br>
    <b>Общее число диалогов:</b> <?=$dialogs;?> <br>
</p>
<p>
    <b>Сообщений сегодня:</b> <?=$messages2;?> <br>
    <b>Диалогов сегодня:</b> <?=$dialogs2;?> <br>
</p>
<hr>
<?php } ?>

<p>
    Приветствие (для тех, кто написал впервые) <br>

    <textarea style="width:100%;" title="" class="var" data-id="__" data-type="greeting"></textarea>
</p>
<hr>
    <p>
        <b>Общие таймеры инстанса</b>
    </p>
    <div id="common-timers">

    </div>
    <button style="margin-top:5px;"
            class="btn btn-default btn-sm"
            onclick="_new_timer('__')">
        Добавить таймер</button>
<hr>

<div class="modal fade" id="screen-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Скриншот</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center id="xscreenshot"><img alt="Скриншот" width="100%" src="<?=$api->getScreenshot();?>" /></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<?php if(isset($showqr)) { ?>
    <!-- Modal -->
    <div class="modal fade" id="qr-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-lg modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">QR-код</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center><img id="screenshot" src="<?=$api->getQRCode();?>" /></center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#qr-modal').modal('show');
    </script>
<?php } ?>

<div id="contents">

</div>

<button class="btn btn-success" onclick="save_all()"><i class="fa fa-save"></i> Сохранить</button>
<button class="btn btn-info" onclick="create_new()"><i class="fa fa-plus"></i> Добавить ответ</button>

<div id="timers-tpl" style="display:none;">
    <div class="row">
        <div class="col-3">
            <input type="text" width="100%" value="%TIME%" placeholder="Время" class="timer-key" data-id="%REQ%">
        </div>
        <div class="col-8">
            <input type="text" width="100%" value="%TEXT%" placeholder="Текст" class="timer-value" data-id="%REQ%">
        </div>
        <div class="col-1">
            <button onclick="this.parentNode.parentNode.remove()">&times;</button>
        </div>
    </div>
</div>

<script>
    var TimerTpl = $("#timers-tpl").html();
    $("#timers-tpl").remove();

    function _new_timer(el, time = "", text = "") {
        if(el === "__") {
            req = '__';
            apto = $('#common-timers')[0];
        }
        else if(typeof el == "string") {
            req = el;
            apto = null;

            $('.timers').each(function(k, v) {
                if(v.dataset['id'] === el)
                    apto = v;
            })
        } else {
            req = el.parentNode.parentNode.children[0].value;
            apto = el.parentNode.children[0];
        }

        $(TimerTpl
            .replace('%TIME%', time)
            .replace('%TEXT%', text)
            .split('%REQ%').join(req)
        ).appendTo(apto);
    }
</script>

<div id="auto-tpl" style="display:none;">
    <div class="row">
        <div class="col-md-4">
            <input type="text" style="width:100%;" class="requests" value="%REQ%" placeholder=""> <br>
            <input type="checkbox" class="flag" data-id="%REQ%" data-type="call"> Обратный звонок<br>
            <input type="checkbox" class="flag" data-id="%REQ%" data-type="lead"> Создать лид<br>
            Вебхук <input type="text" class="var" data-id="%REQ%" data-type="hook"><br>
            Статус лида <input type="text" class="var" data-id="%REQ%" data-type="lead_stage"><br>
            Заголовок лида <input type="text" class="var" data-id="%REQ%" data-type="lead_title"><br>

            <div>
                <div class="timers" data-id="%REQ%"></div>

                <!--<button style="margin-top:5px;"
                        class="btn btn-default btn-sm"
                        onclick="_new_timer(this)">
                    Добавить таймер</button>-->
            </div>
        </div>
        <div class="col-md-7">
            <textarea title="" style="width:100%;height:150px;" class="responses">%RES%</textarea>
        </div>
        <div class="col-md-1">
            <button onclick="this.parentNode.parentNode.remove()">&times;</button>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div>
</div>

<script>
    var AutoInfo = <?=json_encode($auto_inf);?>;
    var AutoFlags = <?=json_encode($auto_flags);?>;
    var AutoTimers = <?=json_encode($auto_timers);?>;
    var AutoVars = <?=json_encode($auto_vars);?>;
    var AutoTpl = $('#auto-tpl').html();
    $('#auto-tpl').remove();

    let tpl = '';

    $.each(AutoInfo, function(k, v) {
        tpl += AutoTpl.split('%REQ%').join(escapeHtml(k)).replace('%RES%', escapeHtml(v));
    });

    $('#contents').html(tpl);

    $.each(AutoTimers, function(k, v) {
        $.each(v, function(kk, val) {
            _new_timer('__', val.time, val.text)
        });
    });

    $('.flag').each(function(k, el) {
        if(AutoFlags[el.dataset['id']] && AutoFlags[el.dataset['id']][el.dataset['type']])
            el.checked = AutoFlags[el.dataset['id']][el.dataset['type']];
    });

    $('.var').each(function(k, el) {
        if(AutoVars[el.dataset['id']] && AutoVars[el.dataset['id']][el.dataset['type']])
            el.value = AutoVars[el.dataset['id']][el.dataset['type']];
    });

    function get_timers(req)
    {
        ans = [];
        tkeys = [];
        tvalues = [];

        $('.timer-key').each(function(k, v) {
            if(v.dataset['id'] === req)
                tkeys.push(v.value);
        });

        $('.timer-value').each(function(k, v) {
            if(v.dataset['id'] === req)
                tvalues.push(v.value);
        });

        tkeys.forEach(function(v, k) {
            ans.push({'time': v, 'text': tvalues[k]});
        });

        return ans;
    }

    function save_all()
    {
        keys = [];
        values = [];
        flags = {};
        vars = {'__': {}};
        timers = {'__': get_timers('__')};

        $('.requests').each(function(idx, e) {
            keys.push(e.value);
            flags[e.value] = {};
            vars[e.value] = {};
            timers[e.value] = get_timers(e.value);
        });

        $('.responses').each(function(idx, e) { values.push(e.value); });

        set = JSON.stringify(els = array_combine(keys, values));

        if(!els)
            return;

        if(!confirm('Сохранить '+keys.length+' элементов?'))
            return;

        $('.flag').each(function(k, el) {
            id = el.parentNode.children[0].value;
            type = el.dataset['type'];
            state = el.checked;

            flags[id][type] = state;
        });

        $('.var').each(function(k, el) {
            if(el.dataset['id'] && el.dataset['id'] == '__')
                vid = '__';
            else
                vid = el.parentNode.children[0].value;

            vtype = el.dataset['type'];
            vstate = el.value;

            vars[vid][vtype] = vstate;
        });

        $.post('', {'setflags': JSON.stringify(flags)}, function() {
            $.post('', {'setvars': JSON.stringify(vars)}, function() {
                $.post('', {'settimers': JSON.stringify(timers)}, function () {
                    $.post('', {'set': set}, function () {
                        location.reload();
                    });
                });
            });
        });
    }

    function create_new() {
        $('#contents').append( AutoTpl.replace('%REQ%', '').replace('%RES%', '') );
    }
</script>