<b>
    Добавление новых сайтов временно осуществляется только через код.
    Для подключения нового клиента обратитесь в IT-отдел
</b>
<hr>
<?php
    if(!isset($ms))
        $ms = new \Astro\Multisite();

    echo '<samp>';
    foreach($ms->sites as $k => $v) {
        if($k == $_SERVER['HTTP_HOST'])
            continue;

        echo "<b><a target='_blank' href='http://{$k}'>{$k}</a></b> | ChatApi {$v['0']}, Bitrix {$v['1']}<hr>";
    }
    echo '</samp>';