<?php
    /**
     * Следующие два блока - ВРЕМЕННЫЙ ВАРИАНТ ЛОГИНА В АДМИНКУ
     * Далеко не идеально, чисто костыль, т.к. пока нет нужды в чём-то большем
     */
    if(isset($_REQUEST['access']))
        $_SESSION['admin_access'] = $_REQUEST['access'];

    if(!isset($_SESSION['admin_access']) || $_SESSION['admin_access'] != 'A1230123')
        die("<h1>Access denied</h1>");

    // Берём список инстансов из общей (adb) базы для того, чтобы отобразить их в главном меню
    $list = \Astro\Mods::$adb->find('instance', []);
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Astronaut Admin</title>

    <!-- Bootstrap core CSS-->
    <link href="/jvendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="/jvendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="/jvendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin.css" rel="stylesheet">

      <!-- Bootstrap core JavaScript-->
      <script src="/jvendor/jquery/jquery.min.js"></script>
      <script src="/jvendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <!-- Core plugin JavaScript-->
      <script src="/jvendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- Page level plugin JavaScript-->
      <script src="/jvendor/chart.js/Chart.min.js"></script>
      <script src="/jvendor/datatables/jquery.dataTables.js"></script>
      <script src="/jvendor/datatables/dataTables.bootstrap4.js"></script>

      <!-- Custom scripts for all pages-->
      <script src="/js/sb-admin.min.js"></script>
      <script>
          function array_combine( keys, values ) {	// Creates an array by using one array for keys and another for its values
              //
              // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

              var new_array = {}, keycount=keys.length, i;

              // input sanitation
              if( !keys || !values || keys.constructor !== Array || values.constructor !== Array ){
                  return false;
              }

              // number of elements does not match
              if(keycount != values.length){
                  return false;
              }

              for ( i=0; i < keycount; i++ ){
                  new_array[keys[i]] = values[i];
              }

              return new_array;
          }

          function escapeHtml(text) {
              var map = {
                  '&': '&amp;',
                  '<': '&lt;',
                  '>': '&gt;',
                  '"': '&quot;',
                  "'": '&#039;'
              };

              return text.replace(/[&<>"']/g, function(m) { return map[m]; });
          }
      </script>
  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="?">Astronaut Admin</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">

      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
          <li class="nav-item">
              <a class="nav-link" href="/">
                  <i class="fas fa-fw fa-info"></i>
                  <span>Главная</span>
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/instances">
                  <i class="fas fa-fw fa-server"></i>
                  <span>Интеграция с Chat-Api</span>
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/azure">
                  <i class="fas fa-fw fa-microsoft"></i>
                  <span>Интеграция с Microsoft Azure</span>
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/webhooks">
                  <i class="fas fa-fw fa-sitemap"></i>
                  <span>Вебхуки</span>
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/platform">
                  <i class="fas fa-fw fa-landmark"></i>
                  <span>Сайты Astronaut</span>
              </a>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-fw fa-list"></i>
                  <span>Инстансы Chat-Api</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                  <h6 class="dropdown-header">Все:</h6>
                  <?php foreach($list as $k => $el) { ?>
                  <a class="dropdown-item" href="/edit?id=<?=$el->_id;?>">Инстанс #<?=$el['id'];?></a>
                  <?php } ?>
              </div>
          </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">