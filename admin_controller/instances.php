<?php
    ini_set('display_errors', true);
    error_reporting(E_ALL);

    if(isset($_POST['s'])) {
        (\Astro\Mods::$adb)->upsert('instance', ['id' => $_POST['s']['id']], $_POST['s']);
    }

    echo '<samp>';
    foreach((\Astro\Mods::$adb)->find('instance', []) as $item) {
        echo $item->id . ':' . $item->url . ' -> ' . $item->token . '<hr>';
    }
    echo '</samp>';
?>

<form action="" method="POST">
    <input type="text" name="s[id]" placeholder="12345" />
    <input type="text" name="s[url]" style="width:450px;" placeholder="https://eu0.chat-api.com/instance12345/" />
    <input type="text" name="s[token]" placeholder="2xk5rt3u3ha3cj42" />
    <input type="submit" value="Attach">
</form>