<table cellspacing="3" border="2" cellpadding="3">
<?php
    try {
        $ms = new \Astro\Multisite();

        foreach($ms->sites as $url => $params) {
            if($url == 'admin.astronaut.kz')
                continue;

            $show = [
                'диалогов' => 'dialogs',
                'сообщений' => 'messages',
                'сделок' => 'deals',
                'лидов' => 'leads',
                'звонков' => 'calls'
            ];

            echo "<tr><td>{$url}</td><td>";
            $db = new \Astro\DB(_DB_CONNECTION, _generate_db_name($url));
            foreach($show as $name => $table)
                echo $db->count($table, []) . ' ' . $name . "\n";
            echo "</td></tr>";
            unset($db);
        }
    }
    catch(Exception $e) {
        print($e);
    }
?>
</table>

