<?php
    if(!defined('ASTRO')) // исключаем прямой доступ
       return;

    /**
     * Вспомогательная функция, определяющая нужную базу данных по домену
     * @param string $domain
     * @todo Поместить в класс
     * @return string
     */
    function _generate_db_name(string $domain)
    {
        return 'astro_' . str_replace('.', '_', $domain);
    }

    // Компоненты Composer
    require_once('vendor/autoload.php');


    if(php_sapi_name() === 'cli') // для cron-скриптов считаем, что клиент в админке
        define('ASTRO_DOMAIN_NAME', 'admin.astronaut.kz');
    elseif($_SERVER['HTTP_HOST'] == 'astronaut.local') // для локального тестирования берём основной сайт
        define('ASTRO_DOMAIN_NAME', 'bb.astronaut.kz');
    else // для остальных смотрим на домен
        define('ASTRO_DOMAIN_NAME', $_SERVER['HTTP_HOST']);

    $ms = new \Astro\Multisite(ASTRO_DOMAIN_NAME);

    // Проверяем, закреплено ли за доменом что-нибудь
    // @todo Убрать html
    if($ms->check() === false ) {
        throw new Exception("<h2>Такой поддомен не зарегистрирован ".ASTRO_DOMAIN_NAME."</h2>");
    }

    // основному домену выводим ошибки прямо в браузер, чтобы быстрее отлавливать
    // остальные же сайты и их пользователи не должны ничего видеть
    if(ASTRO_DOMAIN_NAME === 'bb.astronaut.kz') {
        ini_set('display_errors', true);
        error_reporting(E_ALL);
    }

    /**
     * Данные подключения к базе
     * @todo Вынести в ENV-переменные и изменить заодно
     */
    define('_DB_CONNECTION', 'mongodb://127.0.0.1');
    define('_DB_BASE', _generate_db_name(ASTRO_DOMAIN_NAME));
    define('_REDIS_URL', '127.0.0.1');
    define('_REDIS_PORT', 6380);
    define('_REDIS_PASS', '');

    // если используется CloudFlare - записываем реальный IP-адрес, чтобы не путать с прокси
    if(isset($_SERVER["HTTP_CF_CONNECTING_IP"]))
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];

    // подготовить модули к эксплуатации
    \Astro\Mods::prepare();