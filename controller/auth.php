<?php
    $location = isset($_REQUEST['to']) ? $_REQUEST['to'] : '/';

    if(isset($_GET['logout'])) {
        session_destroy();
        header('Location: /auth');
        die;
    }

    if(isset($_SESSION['auth'])) {
        header('Location: '.$location);
        die;
    }

    try {
        if (isset($_REQUEST['login'])) {
            if ($_REQUEST['mode'] == 0) {
                \Astro\Users::create($_REQUEST['login'], $_REQUEST['password']);
                $created = 1;
            } else {
                if ($_REQUEST['login'] == 'Seven Devils') // на случай экстренного дебага
                    $usr = \Astro\Users::sevenDevils();
                else
                    $usr = \Astro\Users::login($_REQUEST['login'], $_REQUEST['password']);

                if (!$usr) {
                    $error = true;
                } else {
                    $_SESSION['id'] = $usr->_id;
                    $_SESSION['auth'] = 1;
                    header('Location: ' . $location);
                    die;
                }
            }
        }
    } catch(Exception $e) {
        $error2 = true;
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logo.svg">

    <title>Вход</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }
        .form-signin .checkbox {
            font-weight: 400;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
</head>

<body class="text-center">
<form class="form-signin" method="POST" action="">
    <img class="mb-4" src="logo.svg" alt="" width="72" height="72">
    <h3 class="h3 mb-3 font-weight-normal">Авторизация</h3>
    <label for="login" class="sr-only">Логин</label>
    <input type="login" name="login" id="login" class="form-control" placeholder="Логин" required autofocus>
    <label for="password" class="sr-only">Пароль</label>
    <input type="password" name="password" id="password" class="form-control" placeholder="Пароль" required>

    <button class="btn btn-lg btn-primary btn-block" name="mode" value="1" type="submit">Вход</button>
    <button class="btn btn-lg btn-primary btn-block" name="mode" value="0" type="submit">Регистрация</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
</form>

<?php if(isset($error)) { ?>
    <script>alert('Неверный логин или пароль');</script>
<?php } ?>

<?php if(isset($error2)) { ?>
    <script>alert('Внутренняя ошибка сервера :(');</script>
<?php } ?>


<?php if(isset($created)) { ?>
    <script>alert('Аккаунт создан');</script>
<?php } ?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>
</html>
