<?php
    if(isset($_COOKIE['debuginfo'])) {
        ob_start();
        var_dump($_REQUEST);
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        echo "<hr>";
    } else {
        error_reporting(0);
        ini_set('display_errors', '0');
    }

    $ref = strtolower($_REQUEST['PLACEMENT']);

    if(strpos($ref, 'lead')) {
        $table = 'leads';
        $type = 'lead';
    } elseif(strpos($ref, 'deal')) {
        $table = 'deals';
        $type = 'deal';
    } else
        die('Bad referer type');

    $data = json_decode($_REQUEST['PLACEMENT_OPTIONS'], true);

    if(!isset($data['ID']))
        die('Не указан ID');

    $obj = \Astro\Mods::$bitrix->queryForever('crm.'.$type.'.get', [
        "id" => $data['ID']
    ]);

    if(!is_array($obj) || !isset($obj['result']))
        die('Объект не найден в локальной базе. Попробуйте ещё раз через 5 минут');

    $obj = (object)$obj['result'];

    for($i = 0; $i <= 1; $i++) // прохожусь по всем способам извлечения номера, скоро появится больше
    {
        if($i == 0) {
            if(isset($obj->PHONE)) {
                if(is_string($obj->PHONE) && strlen($obj->PHONE))
                    $nums = $obj->PHONE;
                elseif(is_array($obj->PHONE)) {
                    foreach($obj->PHONE as $arr) {
                        if(!isset($arr['VALUE']) || !strlen($arr['VALUE']))
                            continue;

                        $nums = $arr['VALUE'];
                        break;
                    }
                }
            }
        }
        elseif($i == 1) {
            try {
                $contact = $obj->CONTACT_ID;

                if(!is_numeric($obj->CONTACT_ID))
                    continue;

                $contact = \Astro\Mods::$bitrix->queryForever('crm.contact.get', [
                    "id" => $contact
                ]);

                if(isset($contact['result']) && count($contact['result']) && $contact['result']['HAS_PHONE'] == 'Y')
                    $nums = preg_replace("/[^0-9\.]/", '', $contact['result']['PHONE'][0]['VALUE']);
                else
                    continue;
            } catch(\Exception $e) {
                continue;
            }
        }

        if(strlen($nums) >= 8) {
            $ok = true;
            break;
        }
    }

    if(!isset($ok))
        die('Ошибка извлечения номера');

    $nums = preg_replace("/[^0-9\.]/", '', $nums);
    header('Location: /?embed='.$nums);