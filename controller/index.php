<?php
    if(!isset($_SESSION['tracked'])) {
        \Astro\Mods::$db->upsert('track', ['ip' => $_SERVER['REMOTE_ADDR']], [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'cookie' => $_COOKIE
        ]);

        $_SESSION['tracked'] = true;
    }

    if(!isset($_SESSION['auth']) && !isset($_REQUEST['embed'])) {
        header('Location: /auth');
        die;
    }

    if(!isset($_REQUEST['embed'])) {
        $user = new \Astro\Users($_SESSION['id']);

        if (!$user->check()) {
            session_destroy();
            header('Location: /auth.php');
            die;
        }

        $adminLevel = $user->isAdmin();
        $user = $user->get();

        if(!isset($user->role)) {
            $user->role = 1;
        }
    }
    else {
        $user = (object)['role' => 3];
        $adminLevel = false;
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Astronaut</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/pace-theme-flash.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/autosize@4.0.2/dist/autosize.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.10.0/prism.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.7/lib/draggable.bundle.legacy.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@shopify/draggable@1.0.0-beta.7/lib/plugins/swap-animation.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.0/handlebars.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/list.js/1.1.1/list.min.js'></script>
<script src="pace.min.js"></script>
<script src="/js/jquery.ajaxfileupload.js"></script>
<script>
    window.embed = <?=(int)isset($_REQUEST['embed']);?>;

    var stringToColor = function stringToColor(str) {
        var hash = 0;
        var color = '#';
        var i;
        var value;
        var strLength;

        if(!str) {
            return color + '333333';
        }

        strLength = str.length;

        for (i = 0; i < strLength; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }

        for (i = 0; i < 3; i++) {
            value = (hash >> (i * 8)) & 0xFF;
            color += ('00' + value.toString(16)).substr(-2);
        }

        return color;
    };

    var generateAvatar = function generateAvatar(name, el) {
        letter = name.substr(0, 1);

        if(!letter || !letter.match(/[a-zа-яA-ZА-Я]/i)) {
            let alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
            name = alphabet[Math.round(Math.random() * (alphabet.length - 1))];
            letter = name.toUpperCase();
            for(i = 1; i < 5; i++)
                name += alphabet[Math.round(Math.random() * (alphabet.length - 1))];
            name = name.toUpperCase();
        }

        backgroundColor = stringToColor(name);

        el.innerHTML = letter;
        el.style.backgroundColor = backgroundColor;
    };
</script>

<style>
    .btnm {
        margin-right: 5px;
    }
</style>

<div class="layout layout-nav-side">
    <?php if(!isset($_REQUEST['embed'])) { ?>
        <div class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top">
            <a class="navbar-brand" href="#">
                <img src="logolift.png" width="70%" />
            </a>
            <div class="d-flex align-items-center">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse flex-column" id="navbar-collapse">
                <ul class="navbar-nav d-lg-block">
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="getDlgs(false)">Входящие</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="getDlgs(false, 'new')">Непрочитанные (<span id="unread-cnt">0</span>)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Исходящие</a>
                    </li>
                </ul>
                <hr>
                <div class="d-none d-lg-block w-100">
                    <span class="text-small text-muted"></span>
                    <ul class="nav nav-small flex-column mt-2">
                        <li class="nav-item">
                            <a href="ajax?act=logout" class="nav-link">Сменить номер</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" onclick="chat.showScreenshot()" class="nav-link">Скриншот</a>
                        </li>
                        <li class="nav-item">
                            <a href="ajax?act=reboot" class="nav-link">Reboot</a>
                        </li>
                        <?php
                        if($adminLevel === true) {
                            ?>
                            <li class="nav-item">
                                <a target="_blank" href="/admin/index" class="nav-link"><strong>Админ-панель</strong></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <hr>
                </div>
                <div>
                    <div class="dropdown">
                        <button class="btn btn-primary btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Отправить
                        </button>
                        <div class="dropdown-menu" aria-labelledby="newContentButton">
                            <a class="dropdown-item" data-toggle="modal" data-target="#send-phone" href="#send-phone">На номер</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#send-spam" href="#send-spam">Массовая рассылка</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#deals" href="#deals">Мои сделки</a>
                        </div>
                    </div>
                </div>
                <div class="d-none d-lg-block w-100">
                    <span class="text-small text-muted"></span>
                    <ul class="nav nav-small flex-column mt-2">
                        <li class="nav-item" id="online-status">

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="main-container">
        <div class="navbar bg-white breadcrumb-bar">
            <nav aria-label="breadcrumb">
                Чат
            </nav>
        </div>

        <div class="content-container">
            <?php if(!isset($_GET['embed'])) { ?>
                <div class="sidebar collapse" id="sidebar-collapse">
                    <div class="sidebar-content">
                        <div class="chat-team-sidebar text-small">
                            <!--<div class="chat-team-sidebar-top">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h5 class="mb-1"></h5>
                                    </div>
                                </div>ы
                            </div>-->

                            <div class="chat-team-sidebar-bottom">
                                <div class="tab-content">

                                    <script>
                                        function getDlgs(renew, filter) {
                                            if(window.embed == 1)
                                                return;

                                            if(renew && renew == true && window.last_offset)
                                                offset = window.last_offset;
                                            else {
                                                offset = 0;
                                                $('#members-list')[0].innerHTML = '';

                                                if(filter == "new")
                                                    $('#unread-cnt')[0].innerHTML = '0';
                                            }

                                            $.get('/ajax?act=contacts', {'offset': offset}, function(data) {
                                                window._c_names = {};

                                                $.each(data, function(k, v) {
                                                    if(v.offset)
                                                        window.last_offset = v.offset;

                                                    if(!v.chatId)
                                                        return;

                                                    k = v.chatId;

                                                    if(filter && filter == "new" && v.answered == true)
                                                        return;

                                                    if(v.answered == false) {
                                                        addstyle = "background-color:#D6EAF8;";
                                                        $('#unread-cnt')[0].innerHTML = Number($('#unread-cnt')[0].innerHTML) + 1;
                                                    }
                                                    else
                                                        addstyle = "";

                                                    window._c_names[k] = v.name;
                                                    id = k.match(/[0-9]+/)[0];

                                                    htmls = '<a style="'+addstyle+'" class="list-group-item list-group-item-action" onclick="chat.selectDialog(\''+k+'\', \''+v.name+'\')" href="#">\n' +
                                                        '                                            <div class="media media-member mb-0">\n' +
                                                        '                                                <div id="avatar-'+id+'" class="user-info-avatar" /></div>\n' +
                                                        '                                                <div class="media-body">\n' +
                                                        '                                                    <h6 class="mb-0" data-filter-by="text">+'+id+' ~ <font color="grey">'+v.name+'</font></h6>\n' +
                                                        '                                                    <font color="grey">'+v.last+'</font>\n' +
                                                        '                                                </div>\n' +
                                                        '                                            </div>\n' +
                                                        '                                        </a>';

                                                    if(v.answered == false)
                                                        $('#members-list')[0].innerHTML = htmls + $('#members-list')[0].innerHTML;
                                                    else
                                                        $('#members-list')[0].innerHTML += htmls;

                                                    generateAvatar(v.name, $('#avatar-'+id)[0]);
                                                });
                                            }, 'json');
                                        }
                                    </script>

                                    <div class="tab-pane fade show active" id="members" role="tabpanel" aria-labelledby="members-tab" data-filter-list="list-group">
                                        <!--<form class="px-3 mb-3">
                                            <div class="input-group input-group-round">
                                                <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">filter_list</i>
                                                            </span>
                                                </div>
                                                <input type="search" class="form-control filter-list-input" placeholder="Поиск..." aria-label="Filter Members" aria-describedby="filter-members">
                                            </div>
                                        </form>-->

                                        <div id="members-list" class="list-group list-group-flush">

                                        </div>
                                        <script>
                                            $("#members").scroll(function() {
                                                if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
                                                    getDlgs(true);
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="chat-module" data-filter-list="chat-module-body">
                <div class="chat-module-top">
                    <div id="curr-num">

                    </div>
                    <div id="chat-block" class="chat-module-body">

                    </div>
                </div>
                <div class="chat-module-bottom">
                    <form class="chat-form">
                        <textarea id="message-to-send" class="form-control" placeholder="" rows="1"></textarea>
                        <div class="chat-form-buttons">
                            <button id="send" type="button" class="btn btn-primary">
                                Отправить
                            </button>
                            &nbsp;
                            <form action="/ajax" method="POST" enctype="multipart/form-data">
                                <input style="width:0px;height:0px;overflow:hidden;" type="file" id="sendfile" name="file">
                                <input type="hidden" id="targetchat" name="chat" />
                                <button onclick="$('#sendfile').click()" type="button" class="btn btn-primary">
                                    <i class="fa fa-paperclip" style="color:white;"></i>
                                </button>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="send-phone" tabindex="-1" role="dialog" aria-labelledby="sendphonelabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="sendphonelabel">Отправить сообщение на номер</h5>
            </div>
            <div class="modal-body">
                <p class="modal-cont">
                    <input type="text" class="modal-cont-inp" id="num" placeholder="00000000000"> <br>
                    <textarea class="modal-cont-inp" id="numtxt" placeholder="" rows="5"></textarea> <br>
                </p>
                <script>
                    function send_phone_mes()
                    {
                        chat.sendNum( $('#num')[0].value, $('#numtxt')[0].value );
                        alert('Отправлено');
                    }
                </script>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" onclick="send_phone_mes()" class="btn btn-primary">Отправить</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="set-manager" tabindex="-1" role="dialog" aria-labelledby="setmanagerlabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="setmanagerlabel">Отдать сотруднику</h5>
            </div>
            <div class="modal-body">
                <p class="modal-cont">
                    <select class="modal-cont-inp" id="manager">
                        <?php foreach(parse_ini_file('inc/managers.ini') as $k => $v) { ?>
                            <option value="<?=$k;?>"><?=$v;?></option>
                        <?php } ?>
                    </select>
                </p>

                <script>
                    function _set_manager()
                    {
                        let manager = $('#manager')[0].value;

                        $.get('/ajax', {'act': 'setmanager', 'id': chat.selectedDialog, 'manager': manager}, function(ans) {
                            if(ans == 'ok')
                                alert('Готово');
                            else
                                alert(ans);
                        });
                    }
                </script>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" onclick="_set_manager()" class="btn btn-primary">Передать</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="send-spam" tabindex="-1" role="dialog" aria-labelledby="spamlabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="spamlabel">Отправить сообщение на номер</h5>
            </div>
            <div class="modal-body">
                <p class="modal-cont">
                    <input type="text" class="modal-cont-inp" id="nums" placeholder="00000000000,00000000000,00000000000"> <br>
                    <textarea class="modal-cont-inp" id="numstxt" placeholder="" rows="5"></textarea> <br>
                </p>
                <script>
                    function send_phone_mes_spam()
                    {
                        $.each($('#nums')[0].value.split(','), function(k, nm) {
                            chat.sendNum( nm, $('#numstxt')[0].value );
                        });

                        alert('Рассылка начата');
                    }
                </script>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" onclick="send_phone_mes_spam()" class="btn btn-primary">Начать рассылку</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deals" tabindex="-1" role="dialog" aria-labelledby="spamlabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="spamlabel">Мои сделки</h5>
            </div>
            <div class="modal-body">
                <script>
                    function getDeals() {
                        return;

                        if(window.embed == 1)
                            return;

                        $.get('/ajax?act=contacts', {'deals': 1}, function(data) {
                            $.each(data, function(k, v) {
                                if(!v.TITLE.match(/[0-9]+/))
                                    return;

                                $('#deals-list')[0].innerHTML += "<tr><td>"+v.TITLE+"</td><td width='20%'><a href='#' data-dismiss='modal' onclick='chat.selectDialog(\""+v.TITLE.match(/[0-9]+/)[0]+"\", \"Чат\")'>открыть чат</a></td></tr>"
                            });
                        }, 'json');
                    }

                    getDeals();
                </script>

                <table id="deals-list">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<script id="message-template" type="text/x-handlebars-template">
    <div class="media chat-item">
        <img alt="" src="/thumb.jpg" class="avatar" />
        <div class="media-body">
            <div class="chat-item-title">
                <span class="chat-item-author" data-filter-by="text">вы</span>
                <span data-filter-by="text">{{time}}</span>
            </div>
            <div class="chat-item-body" data-filter-by="text">
                <p>{{{messageOutput}}}</p>
            </div>

        </div>
    </div>
</script>

<div class="modal fade" id="qrmodal" tabindex="-1" role="dialog" aria-labelledby="qrlab" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="qrlab">Просканируйте данный код в приложении WhatsApp</h6>
            </div>
            <div class="modal-body">
                <center><img id="qrcode" /></center>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="location.reload()" class="btn btn-default btn-default pull-left" data-dismiss="modal">ОК</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="screenshotmodal" tabindex="-1" role="dialog" aria-labelledby="qrlab" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <center><img width="100%" id="screenshot" /></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-default pull-left" data-dismiss="modal">ОК</button>
            </div>
        </div>
    </div>
</div>

<script id="message-response-template" type="text/x-handlebars-template">
    <div class="media chat-item">
        <div class="user-info-avatar m-avatar-{{id}}">С</div>
        <div class="media-body">
            <div class="chat-item-title">
                <span class="chat-item-author" data-filter-by="text">собеседник</span>
                <span data-filter-by="text">{{time}}</span>
            </div>
            <div class="chat-item-body" data-filter-by="text">
                <p>{{{messageOutput}}}</p>
            </div>

        </div>
    </div>
</script>

<script>
    (function(){

        var chat = {
            messageToSend: '',
            selectedDialog: false,

            init: function() {
                this.cacheDOM();
                this.bindEvents();
                this.render();

                if(window.embed == 1) {
                    setTimeout(function () {
                        location.reload();
                    }, 18000);

                    return;
                }

                this.statusUpdate();
                setInterval(this.statusUpdate, 60000);
            },

            showScreenshot: function() {
                if(window.embed == 1)
                    return;

                $('#screenshot')[0].src = 'ajax?act=screenshot';
                $("#screenshotmodal").modal();
            },

            statusUpdate: function() {
                if(window.embed == 1)
                    return;

                templ = '<br><a class="nav-link" href="#"><i class="fa fa-circle text-%COLOR%"></i> %TEXT%</a>';

                $.get('/ajax', {'act': 'status'}, function(i) {
                    switch(i)
                    {
                        case 'init':
                            color = 'danger';
                            text = 'Клиент не инициализирован';
                            break;

                        case 'error':
                            color = 'danger';
                            text = 'Ошибка: Chat-Api не отвечает';
                            break;

                        case 'loading':
                            color = 'warning';
                            text = 'Идёт загрузка с WhatsApp';
                            break;

                        case 'got qr code':
                            color = 'info';
                            text = 'Получен QR-код';
                            $('#qrcode')[0].src = 'ajax?act=qr';
                            $("#qrmodal").modal();
                            break;

                        case 'authenticated':
                            color = 'success';
                            text = 'Онлайн';
                            break;
                    }

                    $('#online-status')[0].innerHTML = templ.replace('%COLOR%', color).replace('%TEXT%', text);
                });
            },

            cacheDOM: function() {
                this.$chatHistory = $('#chat-block');
                this.$button = $('#send');
                this.$textarea = $('#message-to-send');
                this.$chatHistoryList =  this.$chatHistory.find('.chat-item');
            },

            setstage: function(id) {
                $.get('/ajax', {'act': 'setstage', 'id': id}, function(ans) {
                    if(ans == 'ok')
                        alert('Готово');
                    else
                        alert(ans);
                });
            },

            selectDialog: function(id, name)
            {
                chat.selectedDialog = id;
                $('#targetchat').val(id);

                $('body').unbind('change');
                $("#sendfile").AjaxFileUpload({
                    action: "/ajax?act=sendfile&chat="+id,
                    onComplete: function(filename, response) {
                        if(!response.result)
                            return alert('Что-то пошло не так :(');

                        if(response.result === 'ok')
                            alert('Файл отправлен (в этом окне появится после обработки)');
                        else
                            alert(response.result);
                    }
                });

                this.$chatHistory[0].innerHTML = '';

                $('#curr-num')[0].innerHTML = '+'+id.match(/[0-9]+/)[0] + ' ~ '+name+' ' +
                    <?php if(!isset($_REQUEST['embed'])) { ?>
                    '<a target="_blank" class="btn btnm btn-info float-right" href="ajax?act=lead&chat='+id+'">Открыть в CRM</a>' +
                    <?php } if($user->role == 1) { ?>
                    '<a class="btn btnm btn-info float-right" href="#" onclick="chat.setstage(\''+id+'\')">Распределить</a>' +
                    '<a class="btn btnm btn-info float-right" data-toggle="modal" data-target="#set-manager" href="#set-manager">Отдать</a>' +
                    <?php } ?>
                    ' ';

                $.get('/ajax?act=chat&author='+id, function(data) {
                    console.log(data);

                    $.each(data, function(k, v) {
                        if(!v.id)
                            return;

                        if(v.type == "image")
                            ans = "<img src='"+v.body+"' width=400 />";
                        else if(v.type == "ptt" || v.type == "audio")
                            ans = "<audio controls><source src='"+v.body+"'></audio>";
                        else
                            ans = v.body;

                        if(v.fromMe == true) {
                            chat.getMyMess(ans, (new Date(v.time*1000)).toLocaleString());
                        }
                        else {
                            chat.getMess(v.id, name, ans, (new Date(v.time*1000)).toLocaleString());
                        }
                    });

                    $('.m-avatar-'+id.match(/[0-9]+/)[0]).each(function(k, v) {
                        generateAvatar(name, v);
                    });
                }, 'json');
            },

            getMyMess: function(text, time) {
                var template = Handlebars.compile( $("#message-template").html());
                var context = {
                    messageOutput: text,
                    time: time
                };

                this.$chatHistory.append(template(context));
                this.scrollToBottom();
            },

            getMess: function(id, name, text, time) {
                id = id.match(/[0-9]+/)[0];

                var templateResponse = Handlebars.compile( $("#message-response-template").html());
                var contextResponse = {
                    id: id,
                    messageOutput: text,
                    time: time
                };

                this.$chatHistory.append(templateResponse(contextResponse));
                this.scrollToBottom();
            },

            bindEvents: function() {
                this.$button.on('click', this.addMessage.bind(this));
                this.$textarea.on('keyup', this.addMessageEnter.bind(this));
            },

            render: function() {
                this.scrollToBottom();
                if (this.messageToSend.trim() !== '') {
                    var template = Handlebars.compile( $("#message-template").html());
                    var context = {
                        messageOutput: this.messageToSend,
                        time: this.getCurrentTime()
                    };

                    this.$chatHistory.append(template(context));
                    this.scrollToBottom();
                    this.$textarea.val('');
                }

            },

            sendNum: function(number, text) {
                $.post('/ajax', {'act': 'sendPhone', 'text': text, 'chat': number}, function(ans) {
                    if(ans !== 'ok')
                        return alert('Ошибка при отправке на '+number);
                });
            },

            addMessage: function() {
                if(chat.selectedDialog == false)
                    return alert('Не выбран диалог');

                tx = this.$textarea.val();

                if(!isNaN(parseFloat(chat.selectedDialog)) && isFinite(chat.selectedDialog)) {
                    this.sendNum(chat.selectedDialog, tx);
                    chat.messageToSend = tx;
                    chat.render();
                    return;
                }

                $.post('/ajax', {'act': 'send', 'text': tx, 'chat': chat.selectedDialog}, function(ans) {
                    if(ans !== 'ok')
                        return alert('Ошибка');

                    chat.messageToSend = tx;
                    chat.render();
                });
            },

            addMessageEnter: function(event) {
                if (event.keyCode === 13) {
                    this.addMessage();
                }
            },
            scrollToBottom: function() {
                this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
            },
            getCurrentTime: function() {
                return new Date().toLocaleTimeString().
                replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
            }

        };

        chat.init();

        window.chat = chat;
        <?php if(isset($_GET['sel'])) { ?>
        chat.selectDialog('<?=$_GET['sel'];?>', '');
        getDlgs();
        <?php } elseif(isset($_GET['embed'])) { ?>
        chat.selectDialog('<?=$_GET['embed'];?>', '');
        <?php } else { ?>
        getDlgs();
        <?php } ?>
    })();
</script>
</body>
</html>
