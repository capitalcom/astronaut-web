<?php
    $input = json_decode(file_get_contents('php://input'), true);

    if(!is_array($input) || !count($input))
        die('error:empty_hook');

    if(!is_object($input['messages']) && !is_array($input['messages']))
        die('error:nomessages');

    $messages = &\Astro\Mods::$messages;

    if(isset($_use_webhook_instance))
        $instance = $_use_webhook_instance;
    else
        $instance = 0;

    foreach ($input['messages'] as $k => $v) {
        try {
            $messages->handleNew($v, true, $instance);
        } catch(Exception $e) {
            $messages->send_debug_message( (string)$e );
        }
    }

    die('ok');