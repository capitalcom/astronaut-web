<?php
    /*if($_SERVER['argc'] <= 1 && file_exists('cron_lock.txt') && file_get_contents('cron_lock.txt') >= time()) {
        echo 'Process locked';
        return;
    }

    file_put_contents('cron_lock.txt', time()+115);*/

    echo 'Init Astro...'.PHP_EOL;
    $mess = &\Astro\Mods::$messages;

    if(in_array('dialogs', $_SERVER['argv'])) {
        var_dump( $mess->updateDialogs() );
        return;
    }

    echo 'Refreshing messages DB...' . PHP_EOL;

    $ignore = 0;
    $api = \Astro\Mods::$api;
    $inbox = $api->getInbox();
    echo count($inbox) . ' inbox messages' . PHP_EOL;

    if(is_array($inbox)) {
        foreach ($inbox as $i) {
            if($res = $mess->handleNew('', $i))
                echo $i['id'] . ' inserted' . PHP_EOL;
            else
                $ignore++;
        }
    }

    echo "Ignored {$ignore} messages" . PHP_EOL;