<?php
    if(isset($_SESSION['auth'])) {
        $user = new \Astro\Users($_SESSION['id']);

        if(is_numeric($timst = $user->getTeamInstance())) {
            try {
                \Astro\Mods::prepare($timst);
            }
            catch(Exception $e){
                echo $e;
            }
        }
    }

    $api = &\Astro\Mods::$api;
    $db = &\Astro\Mods::$db;
    $bitrix = &\Astro\Mods::$bitrix;
    $mess = &\Astro\Mods::$messages;

    /*if(isset($_REQUEST['_new'])) {
        var_dump($api->getWebhook());
        echo PHP_EOL;
        $res = $api->setWebhook('https://astronaut.kz/whatsapp/_'.$ms->getIID().'.php');
        die( var_export($res) );
    }
    
    if(isset($_REQUEST['webhook'])) {
        if($_REQUEST['webhook'] == 'bitrix') {
            $debug = [];

            $debug['input'] = $input = json_decode(file_get_contents('php://input'), true);

            if(!is_array($input) || !count($input))
                die('empty_hook');

            foreach($input['messages'] as $k => $v) {
                $debug['handle'] = $mess->handleNew($v, $api);
            }

            file_put_contents('inc/webhook.txt', var_export($debug, true));
            die('ok');
        }
        elseif($_REQUEST['webhook'] == 'bitrix_chat') {
            require_once(__DIR__ . '/inc/bitrix.php');
            die('ok');
        }
        elseif($_REQUEST['webhook'] == 'install_whatsapp') {
            var_export( $api->getWebhook() );
            usleep(50);
            echo '<hr>';
            $res = $api->setWebhook('http://' . $_SERVER['SERVER_NAME'] . '/webhook.php');
            die( var_export($res) );
        }

        $hook = $db->findOne('webhooks', ['url' => $_REQUEST['webhook']]);

        if(!$hook)
            die('not_found');

        die(
        ($api->sendPhoneMessage($hook->to, $hook->text) ? 'ok' : 'fail')
        );
    }*/


    /*if(isset($_SESSION['auth'])) {
        $user = new \Astro\Users($_SESSION['id']);

        if(!$user->check()) {
            session_destroy();
            header('Location: /auth.php');
            die;
        }
    }*/

    // ----------------------------------------------------------------
    switch(@$_REQUEST['act'])
    {
        case 'contacts':
            header('Content-Type: application/json');

            /*if(isset($_REQUEST['as']))
                $user = new \Astro\Users(new MongoDB\BSON\ObjectId($_REQUEST['as']));
            else
                $user = new \Astro\Users($_SESSION['id']);

            if(!$user->check()) {
                session_destroy();
                header('Location: /auth.php');
                die;
            }

            if(!is_numeric($user->role))
                $user->role = 3;

            $role = isset($_REQUEST['test']) ? $_REQUEST['test'] : $user->role;

            $role = 1;

            switch($role)
            {
                case 1:
                    $query = [];
                    break;

                case 2:
                case 3:
                    $bids = [];
                    $chats = [];
                    $deals = [];

                    if($user->role == 3)
                        $find = ['ASSIGNED_BY_ID' => ['$in' => [(string)$user->bid, (int)$user->bid]]];
                    else {
                        $ids = [];
                        $team = $db->find('users', ['team' => (string)$user->_id])->toArray();

                        foreach($team as $t)
                            if(!is_null($t['bid']))
                                $ids[] = $t['bid'];

                        $find = ['STATUS_ID' => 'C17:4', 'ASSIGNED_BY_ID' => ['$in' => $ids]];
                    }

                    $deals = $db->find('leads', $find)->toArray();

                    if(isset($_REQUEST['leads']))
                        die(json_encode($deals));

                    $numSearch = [];

                    foreach(array_reverse($deals) as $obj) {
                        $nums = preg_replace("/[^0-9\.]/", '', $obj->TITLE);

                        if(strlen($nums) < 10)
                            continue;

                        $numSearch[] = ['chatId' => new MongoDB\BSON\Regex($nums)];
                    }

                    $mine = $db->find('n_taken', ['user' => (string)$_SESSION['id']])->toArray();

                    foreach($mine as $obj) {
                        if(strlen($obj->chat))
                            $numSearch[] = ['chatId' => new MongoDB\BSON\Regex($obj->chat)];
                    }

                    $query = ['$or' => $numSearch];
                    break;

                default:
                    $query = [];
                    break;
            }*/

            $contacts = $mess->getDialogs(isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0, []);
            die(json_encode($contacts));

        case 'lead':
            $data = $db->findOne('bitrix_connection', ['chat' => new MongoDB\BSON\Regex($_REQUEST['chat'])]);
            if(!is_object($data) || !is_numeric($data->lead))
                die('<h1>Сделка не найдена</h1>');

            header('Location: https://'.''.'/crm/lead/details/'.$data->lead.'/');
            die();

        case 'sendfile':
            if(isset($_FILES['file']))
                $file = $_FILES['file'];
            else
                die(json_encode(['result' => 'No file specified']));

            $body = "data:" . $file['type'] . ";base64," . base64_encode(file_get_contents($file['tmp_name']));
            $fn = $file['name'];

            die(
                json_encode(['result' => ($api->sendFile($_REQUEST['chat'], $body, $fn) == true) ? 'ok' : 'Error while sending'])
            );
            # data:image/jpeg;base64,

        case 'setmanager':
            $data = $db->findOne('bitrix_connection', ['chat' => new MongoDB\BSON\Regex($_REQUEST['id'])]);
            if(!is_object($data) || !is_numeric($data->lead))
                die('<h1>Сделка не найдена</h1>');

            $bitrix->queryForever('crm.lead.update', [
                "id" => $data->lead,
                "fields" => ["ASSIGNED_BY_ID" => $_REQUEST['manager']],
                "params" => ["REGISTER_SONET_EVENT" => "Y"]
            ], $auth);

            $db->update('leads', ['ID' => $data->lead], ['ASSIGNED_BY_ID' => $_REQUEST['manager']]);
            die('ok');

        case 'setstage':
            $data = $db->findOne('bitrix_connection', ['chat' => new MongoDB\BSON\Regex($_REQUEST['id'])]);

            if(!is_object($data) || !is_numeric($data->lead))
                die('<h1>Сделка не найдена</h1>');

            $bitrix->queryForever('crm.lead.update', [
                "id" => $data->lead,
                "fields" => ["ASSIGNED_BY_ID" => $_wt_invite[0]],
                "params" => ["REGISTER_SONET_EVENT" => "Y"]
            ], $auth);

            $db->update('leads', ['ID' => $data->lead], ["ASSIGNED_BY_ID" => $_wt_invite[0]]);
            die('ok');

        case 'new_session':
            session_destroy();
            header('Location: /');
            die('ok');

        case 'logout':
            $api->logout();
            $api->reboot();
            header('Location: /');
            die('ok');

        case 'reboot':
            $api->reboot();
            header('Location: /');
            die('ok');

        case 'status':
            die( $api->getStatus() );

        case 'qr':
            header('Content-Type: image/png');
            readfile( $api->getQRCode() );
            die;

        case 'screenshot':
            header('Content-Type: image/png');
            readfile( $api->getScreenshot() );
            die;

        case 'export':
            $box = $api->getFullInbox();
            $nums = [];
            foreach($box as $b) {
                $n = trim(explode('-', explode('@', $b['chatId'])[0])[0]);
                $nums[$n] = $n;
            }

            echo '<h3>'.count($box).' (~'.count($nums).')</h3>' . implode('<br>', $nums);

            die();

        case 'testtaken':
            var_dump($db->find('n_taken', [])->toArray());
            die;

        case 'sendPhone':
            if(isset($_SESSION['id'])) {
                $rec = $db->find('n_taken', ['user' => (string)$_SESSION['id'], 'chat' => (string)$_REQUEST['chat']])->toArray();

                if(!count($rec))
                    $db->insert('n_taken', ['user' => (string)$_SESSION['id'], 'chat' => (string)$_REQUEST['chat']]);
            }

            die(
                ($api->sendPhoneMessage($_REQUEST['chat'], $_REQUEST['text']) == true) ? 'ok' : 'fail'
            );

        case 'send':
            die(
                ($api->sendMessage($_REQUEST['chat'], $_REQUEST['text']) == true) ? 'ok' : 'fail'
            );

        case 'install':
            $wh = $api->setWebhook();
            die(json_encode(['webhook' => $wh]));

        case 'mass':
            header('Content-Type: application/json');

            $apis = [];
            $usrs = $db->find('users', []);

            foreach($usrs as $kk => $vv) {
                if(isset($vv['w_token']) && strlen($vv['w_url']) > 5) {
                    $apis[] = new \Astro\ChatApi(
                        $vv['w_token'], // Chat-Api.com token
                        $vv['w_url'] // Chat-Api.com API url
                    );
                }
            }

            $phones = explode(',', $_REQUEST['phones']);
            $ok = 0;

            foreach($phones as $kp => $ph) {
                $sel = rand(0, count($apis) - 1);
                if( $apis[$sel]->sendPhoneMessage($ph, $_REQUEST['text']) )
                    $ok++;
            }

            die( json_encode(['sent_messages' => $ok]) );

        case 'chat':
            header('Content-Type: application/json');
            $msgs = $mess->getChatMessages(@$_GET['author']);
            die(json_encode($msgs));

        case 'inbox':
            header('Content-Type: application/json');
            die(json_encode($mess->getInbox(isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0)->toArray()));

        default:
            die('No action');
    }