<?php
    if(!isset($_SESSION['auth'])) {
        header('Location: /auth');
        return;
    }

    $user = new \Astro\Users($_SESSION['id']);

    if(!$user->check()) {
        header('Location: /');
        return;
    }

    if(!$user->isAdmin()) {
        header('Location: /');
        return;
    }

    $user = $user->get();

    if(!isset($ms))
        $ms = new \Astro\Multisite(ASTRO_DOMAIN_NAME);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Astronaut</title>

    <!-- Bootstrap core CSS-->
    <link href="/jvendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="/jvendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="/jvendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript-->
    <script src="/jvendor/jquery/jquery.min.js"></script>
    <script src="/jvendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/jvendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="/jvendor/chart.js/Chart.min.js"></script>
    <script src="/jvendor/datatables/jquery.dataTables.js"></script>
    <script src="/jvendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin.min.js"></script>
    <script>
        function array_combine( keys, values ) {	// Creates an array by using one array for keys and another for its values
            //
            // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

            var new_array = {}, keycount=keys.length, i;

            // input sanitation
            if( !keys || !values || keys.constructor !== Array || values.constructor !== Array ){
                return false;
            }

            // number of elements does not match
            if(keycount != values.length){
                return false;
            }

            for ( i=0; i < keycount; i++ ){
                new_array[keys[i]] = values[i];
            }

            return new_array;
        }

        function escapeHtml(text) {
            var map = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#039;'
            };

            return text.replace(/[&<>"']/g, function(m) { return map[m]; });
        }
    </script>
</head>

<body id="page-top">

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="?">Astronaut</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">

    </ul>

</nav>

<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-robot"></i>
                <span>Автоответчик</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <a class="dropdown-item" href="/admin/index">Основной #<?=($main = $ms->detect()[0]);?></a>
                <h6 class="dropdown-header">Все:</h6>
                <?php foreach($ms->detect()[3] as $el) { if($el == $main) continue; ?>
                    <a class="dropdown-item" href="/admin/index?id=<?=$el;?>">Инстанс #<?=$el;?></a>
                <?php } ?>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/accounts">
                <i class="fas fa-fw fa-users"></i>
                <span>Пользователи</span>
            </a>
        </li>
        <?php if(ASTRO_DOMAIN_NAME == 'bb.astronaut.kz') { ?>
            <li class="nav-item">
                <a class="nav-link" href="/admin/funnel">
                    <i class="fas fa-fw fa-filter"></i>
                    <span>Воронка продаж</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/teams">
                    <i class="fas fa-fw fa-user-astronaut"></i>
                    <span>Команды</span>
                </a>
            </li>
        <?php } ?>
    </ul>

    <div id="content-wrapper">

        <div class="container-fluid">