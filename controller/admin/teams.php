<?php
    if(isset($_REQUEST['new_team']))
    {
        \Astro\Mods::$db->insert('teams', [
            'name' => $tn = $_REQUEST['new_team']
        ]);

        \Astro\Mods::$messages->send_debug_message('🚐 '.ASTRO_DOMAIN_NAME.' : новая команда «'.$tn.'»');
        print('ok');
        return;
    }

    if(isset($_REQUEST['del'])) {
        \Astro\Mods::$db->delete('teams', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['del'])]);

        print('ok');
        return;
    }

    if(isset($_REQUEST['setinst'])) {
        \Astro\Mods::$db->updateOne('teams', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['id'])], ['instance' => $_REQUEST['setinst']]);

        print('ok');
        return;
    }

    require_once(__DIR__ . '/_header.php');
?>
    <script>
        function _newteam()
        {
            if(!confirm('Добавить новую команду?'))
                return 0;

            if(!(tn = prompt('Как вы хотите назвать команду?')))
                return 0;

            $.post('', {'new_team': tn}, function(d) {
                location.reload();
            });

            return 1;
        }

        function _setinst(id, inst)
        {
            $.post('', {'id': id, 'setinst': inst}, function(d) {
                location.reload();
            });

            return 1;
        }

        function _delteam(task)
        {
            if(!confirm('Точно удалить команду?'))
                return 0;

            $.post('', {'del': task}, function(d) {
                location.reload();
            });

            return 1;
        }
    </script>

    <button onclick="_newteam()" class="btn btn-primary">Создать команду</button>
    <hr>
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Инстанс</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach(\Astro\Mods::$db->find('teams', []) as $k => $usr) { ?>
            <tr>
                <td><?=$usr->_id;?></td>
                <td width="35%">
                    <?=$usr->name;?>
                </td>
                <td width="35%">
                    <select width="100%" onchange="_setinst('<?=$usr->_id;?>', this.value)">
                        <option>инстанс...</option>
                        <?php foreach(\Astro\Mods::$ms->getMyIIDs() as $v) { ?>
                            <option <?=(isset($usr->instance) && $usr->instance == $v) ? 'selected="selected"' : '';?> value="<?=$v;?>"><?=$v;?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <button class="btn btn-danger" onclick="_delteam('<?=$usr->_id;?>')">&times;</button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
require_once(__DIR__ . '/_footer.php');