<?php
    if(isset($_REQUEST['new_task']))
    {
        \Astro\Mods::$db->insert('funnel_tasks', [
            'type' => 'funnel',
            'desc' => ($desc = 'Сформировать воронку продаж'),
            'time' => time(),
            'from' => $_REQUEST['from'],
            'to' => $_REQUEST['to'],
            'update' => time(),
            'status' => 0
        ]);

        \Astro\Mods::$messages->send_debug_message('⚒ '.ASTRO_DOMAIN_NAME.' : новая задача «'.$desc.'»');
        print('ok');
        return;
    }

    if(isset($_REQUEST['result'])) {
        $f = \Astro\Mods::$db->findOne('funnel_tasks', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['result'])]);

        if(!isset($f->result)) {
            print('Задача либо еще не завершена, либо не существует');
        } else {
            print(base64_decode($f->result));
        }

        return;
    }

    if(isset($_REQUEST['del'])) {
        \Astro\Mods::$db->delete('funnel_tasks', ['_id' => new \MongoDB\BSON\ObjectId($_REQUEST['del'])]);

        print('ok');
        return;
    }

    require_once(__DIR__ . '/_header.php');
?>
    <script>
        function _create_funnel()
        {
            if(!confirm('Будет запущен процесс формирования воронки продаж. Продолжить?'))
                return 0;

            $.post('', {'new_task': 1, 'from': $('#from').val(), 'to': $('#to').val()}, function(d) {
                location.reload();
            });

            return 1;
        }

        function _delete_task(task)
        {
            if(!confirm('Точно удалить задачу? Если она находится в процессе, это может привести к ошибкам'))
                return 0;

            alert('Нет гарантий, что задача не выполняется, если её статус выше "Ожидается выполнения"');

            $.post('', {'del': task}, function(d) {
                location.reload();
            });

            return 1;
        }
    </script>

    с
    <input type="date" id="from">
    по
    <input type="date" id="to">

    <button onclick="_create_funnel()" class="btn btn-primary">Создать задачу</button>
    <hr>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Описание задачи</th>
                <th>Статус</th>
                <th>Создано / Обновлено</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach(\Astro\Mods::$db->find('funnel_tasks', ['type' => 'funnel']) as $k => $usr) { ?>
            <tr>
                <td><?=$usr->_id;?></td>
                <td>
                    <?=$usr->desc;?>
                    <?php if(isset($usr->from)) { ?>
                        <br>
                        <i>период: с <?=$usr->from;?> по <?=$usr->to;?></i>
                    <?php } ?>
                </td>
                <td>
                    <?php
                        switch($usr->status)
                        {
                            case -1:
                                print('Забракована');
                                break;

                            case 0:
                                print('Ожидается выполнение');
                                break;

                            case 1:
                                print('Выполняется');
                                break;

                            case 5:
                                print('Завершена [<a target="_blank" href="?result='.$usr->_id.'">результат</a>]');
                                break;

                            default:
                                print('?');
                        }
                    ?>
                </td>
                <td><?=date('d.m.Y H:i', $usr->time);?><br><?=date('d.m.Y H:i', $usr->update);?></td>
                <td>
                    <button class="btn btn-danger" onclick="_delete_task('<?=$usr->_id;?>')">&times;</button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
    require_once(__DIR__ . '/_footer.php');