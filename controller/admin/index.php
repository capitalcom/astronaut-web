<?php
    require_once(__DIR__ . '/_header.php');

    // страница частично выполнена костыльно, потом @todo доработать как будет время

    $ms = new \Astro\Multisite();

    if(!isset($_GET['id']))
        $instance = $ms->getIID();
    else {
        $instance = (int)$_GET['id'];

        if(!in_array($instance, $ms->detect()[3]))
            throw new \Astro\AccessException("Нет доступа к данному инстансу");
    }

    if(!is_numeric($instance) || $instance <= 0)
        print("Не удалось определить нужный экземпляр WhatsApp");
    else {
        $_id = \Astro\Mods::$adb->findOne('instance', ['id' => (string)$instance]);

        if(!is_object($_id) || !isset($_id->_id)) {
            print("Не удалось найти нужный экземпляр WhatsApp");
        } else {
            $_REQUEST['id'] = (string)$_id->_id;
            require_once(__DIR__ . '/../../admin_controller/edit.php');
        }
    }

    require_once(__DIR__ . '/_footer.php');