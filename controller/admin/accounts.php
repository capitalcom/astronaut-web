<?php
    require_once(__DIR__ . '/_header.php');

    /**
     * Управление аккаунтами
     *
     * Насчёт средненького качества кода: это перенесено из старого Астронавта как рабочий вариант
     * Т.е. оно работает, но не выглядит идеально, а посему:
     * @todo Переписать, когда будет больше времени
     * @todo Предусмотреть все изменяемые переменные и делать валидацию, а не подставлять всё в запрос напрямую
     */

    $db = \Astro\Mods::$db;

    if(isset($_REQUEST['del'])) {
        $db->delete('users', ['login' => $_REQUEST['del']]);
        header('Location: ?ok');
        die;
    }

    if(isset($_REQUEST['login'])) {
        if($_REQUEST['key'] == 'password')
            $_REQUEST['value'] = md5($_REQUEST['value']);

        $db->update('users', ['login' => $_REQUEST['login']], [$_REQUEST['key'] => $_REQUEST['value']]);
        header('Location: ?ok');
        die;
    }
?>

    <script type="text/javascript">
        $(document).ready(function(){
            // Activate tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Select/Deselect checkboxes
            var checkbox = $('table tbody input[type="checkbox"]');
            $("#selectAll").click(function(){
                if(this.checked){
                    checkbox.each(function(){
                        this.checked = true;
                    });
                } else{
                    checkbox.each(function(){
                        this.checked = false;
                    });
                }
            });
            checkbox.click(function(){
                if(!this.checked){
                    $("#selectAll").prop("checked", false);
                }
            });
        });
    </script>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Логин</th>
                <td>Роль</td>
                <th>Токены</th>
                <th>Доступ к админке</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($db->find('users', []) as $k => $usr) { ?>
                <tr>
                    <td>
                        <?=$usr->_id;?>
                    </td>
                    <td><a href="#" onclick="_set_v('<?=$usr->login;?>', 'login')"><?=$usr->login;?></a></td>
                    <td>
                        <select onchange="_set_value('<?=$usr->login;?>', 'role', this.value)">
                            <?php foreach([1 => 'Ресепшн менеджер', 2 => 'WhatsApp-менеджер', 3 => 'Хантер'] as $i => $v) { ?>
                                <option <?=(isset($usr->role) && $usr->role == $i) ? 'selected="selected"' : '';?> value="<?=$i;?>"><?=$v;?></option>
                            <?php } ?>
                        </select>
                        <?php
                        if(ASTRO_DOMAIN_NAME == 'bb.astronaut.kz') {
                            ?>
                            <br>
                            <select onchange="_set_value('<?=$usr->login;?>', 'team', this.value)">
                                <option>команда...</option>
                                <?php foreach($db->find('teams', []) as $i => $v) { ?>
                                    <option <?=(isset($usr->team) && $usr->team == $v->_id) ? 'selected="selected"' : '';?> value="<?=$v->_id;?>">Команда <?=$v->name;?></option>
                                <?php } ?>
                            </select>
                        <?php } ?>
                    </td>
                    <td>
                        URL: <?=isset($usr->w_url) ? $usr->w_url : '-';?> <a href="#" onclick="_set_v('<?=$usr->login;?>', 'w_url')">[изменить]</a>
                        <br>
                        Token: <?=isset($usr->w_token) ? $usr->w_token : '-';?> <a href="#" onclick="_set_v('<?=$usr->login;?>', 'w_token')">[изменить]</a>
                        <br>
                        Bitrix ID: <?=isset($usr->bid) ? $usr->bid : '-';?> <a href="#" onclick="_set_v('<?=$usr->login;?>', 'bid')">[изменить]</a>
                    </td>
                    <td>
                        <?php if($usr->login === 'admin') { ?>
                        <input type="checkbox" disabled="disabled" checked="checked"/>
                        <?php } else { ?>
                        <input type="checkbox" onchange="set_admin('<?=$usr->login;?>', <?=!(isset($usr->admin) && $usr->admin > 0);?>)" <?=(isset($usr->admin) && $usr->admin > 0) ? 'checked' : '';?>/>
                        <?php } ?>
                    </td>
                    <td>
                        <a href="?del=<?=$usr->login;?>" class="delete btn btn-danger">Удалить &times;</a> <hr>
                        <a href="#" onclick="_set_v('<?=$usr->login;?>', 'password')">[установить пароль]</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

<script>
    function set_admin(id, news) {
        $.post('', {'key': 'admin', 'value': news, 'login': id}, function() {
            location.reload();
        });
    }

    function _set_value(id, key, value) {
        $.post('', {'key': key, 'value': value, 'login': id}, function() {
            location.reload();
        });
    }

    function _set_v(id, key) {
        if(!(value = prompt('Введите новое значение')))
            return;

        $.post('', {'key': key, 'value': value, 'login': id}, function() {
            location.reload();
        });
    }
</script>

<?php require_once(__DIR__ . '/_footer.php'); ?>